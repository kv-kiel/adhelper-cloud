import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {AuthenticationGuard} from './core/guards/authentication.guard';
import {UserRole} from './models/user';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {AuthorizationGuard} from './core/guards/authorization.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthenticationGuard]},
  {path: 'settings/changePassword', component: ChangePasswordComponent, canActivate: [AuthenticationGuard]},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'user', canActivate: [AuthenticationGuard, AuthorizationGuard], data: {roles: [UserRole.Admin]},
    loadChildren: () => import ('./modules/user/user.module').then(m => m.UserModule)},
  {path: 'member', canActivate: [AuthenticationGuard, AuthorizationGuard], data: {roles: [UserRole.Admin, UserRole.Assessor]},
    loadChildren: () => import ('./modules/member/member.module').then(m => m.MemberModule)},
  {path: 'labour', canActivate: [AuthenticationGuard, AuthorizationGuard],
    data: {roles: [UserRole.Admin, UserRole.Typist, UserRole.Assessor]},
    loadChildren: () => import ('./modules/labour/labour.module').then(m => m.LabourModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
