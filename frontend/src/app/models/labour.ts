import {IMember} from './member';

export interface IWorkHour {
  id?: number;
  date: Date;
  member?: IMember;
  memberId?: number;
  hours: number;
}

export interface IAttendee extends IWorkHour{
  id?: number;
  date: Date;
  hours: number;
  member: {
    memberId: number;
    firstName: string;
    lastName: string;
  };
}
