import {UserRole} from './user';

export interface ISession {
  username: string;
  expiry: Date;
  maxAgeSeconds: number;
  userRole: UserRole;
}
