export interface IParseResult {
    rowCount: number;
    invalidCount: number;
    invalidRows: number[];
}
