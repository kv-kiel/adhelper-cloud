import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {

  @Input() title: string = '';
  @Input() message: string = '';
  @Input() cancel: string | undefined = undefined;
  @Input() confirm: string = 'Ok';

  @Output() resultEvent = new EventEmitter<'confirm' | 'cancel'>();

  constructor() {}

  onClick(result: 'confirm' | 'cancel'): void {
    this.resultEvent.emit(result);
  }
}
