import {Component, ComponentFactory, Input, OnInit, ViewChild} from '@angular/core';
import {ModalHostDirective} from '../modal-host.directive';
import {ModalService} from '../modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @ViewChild(ModalHostDirective, {static: true}) appModalHost!: ModalHostDirective;
  @Input() componentFactory: ComponentFactory<unknown> | undefined = undefined;

  isModalVisible: boolean = false;
  hasBackdrop: boolean = false;

  constructor(
    private modalService: ModalService
  ) { }

  ngOnInit(): void {
    this.modalService.getModalVisible().pipe().subscribe(next => this.isModalVisible = next);
    this.modalService.getModalBackdrop().pipe().subscribe(next => this.hasBackdrop = next);
    this.modalService.setContainerRef(this.appModalHost.viewContainerRef);
  }
}
