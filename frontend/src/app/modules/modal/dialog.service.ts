import {ComponentFactory, ComponentFactoryResolver, Injectable} from '@angular/core';
import {ModalService} from './modal.service';
import {DialogComponent} from './dialog/dialog.component';
import {take, tap} from 'rxjs/operators';
import {EMPTY, Observable, of} from 'rxjs';
import {MemberUploadComponent} from '../member/member-upload/member-upload.component';
import {AttendeeFormComponent} from '../labour/attendee-form/attendee-form.component';
import {IAttendee} from '../../models/labour';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }

  public showDialog(title: string, message: string, confirm: string, cancel?: string): Observable<'confirm' | 'cancel'> {
    const componentFactory: ComponentFactory<DialogComponent> = this.componentFactoryResolver.resolveComponentFactory(DialogComponent);
    const ref = this.modalService.showModal(componentFactory);
    if (ref) {
      ref.instance.title = title;
      ref.instance.message = message;
      ref.instance.confirm = confirm ;
      ref.instance.cancel = cancel;
      return ref.instance.resultEvent.pipe(
        take(1),
        tap(() => this.modalService.hideModal(ref)),
      );
    }
    return EMPTY;
  }

  public showUploadDialog(): Observable<boolean> {
    const componentFactory: ComponentFactory<MemberUploadComponent> =
      this.componentFactoryResolver.resolveComponentFactory(MemberUploadComponent);
    const ref = this.modalService.showModal(componentFactory);
    if (ref) {
      return ref.instance.resultEvent.pipe(
        take(1),
        tap(() => this.modalService.hideModal(ref)),
      );
    }
    return of(false);
  }

  public showAttendeeDialog(date: string, attendee?: IAttendee): Observable<boolean> {
    const componentFactory: ComponentFactory<AttendeeFormComponent> =
      this.componentFactoryResolver.resolveComponentFactory(AttendeeFormComponent);
    const ref = this.modalService.showModal(componentFactory);
    if (ref) {
      ref.instance.date = date;
      ref.instance.attendee = attendee;
      return ref.instance.resultEvent.pipe(
        take(1),
        tap(() => this.modalService.hideModal(ref)),
      );
    }
    return of(false);
  }
}
