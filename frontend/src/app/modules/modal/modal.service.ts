import {ComponentFactory, ComponentRef, Injectable, ViewContainerRef} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private modalVisibleSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private modalBackdropSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  private viewContainerRef: ViewContainerRef | undefined;

  constructor(
  ) {
  }

  public getModalVisible(): Observable<boolean> {
    return this.modalVisibleSubject.asObservable();
  }

  public getModalBackdrop(): Observable<boolean> {
    return this.modalBackdropSubject.asObservable();
  }

  public showModal<C>(componentFactory: ComponentFactory<C>): ComponentRef<C> | undefined {
    if (this.viewContainerRef) {
      this.modalVisibleSubject.next(true);
      this.viewContainerRef.clear();
      return this.viewContainerRef.createComponent(componentFactory);
    }
    return undefined;
  }

  public hideModal<C>(ref: ComponentRef<C>): void {
    this.modalVisibleSubject.next(false);
    this.viewContainerRef?.clear();
    ref.destroy();
  }

  public setContainerRef(ref: ViewContainerRef): void {
    this.viewContainerRef = ref;
  }
}
