import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal/modal.component';
import { ModalHostDirective } from './modal-host.directive';
import { DialogComponent } from './dialog/dialog.component';



@NgModule({
  declarations: [ModalComponent, ModalHostDirective, DialogComponent],
  exports: [
    ModalComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ModalModule { }
