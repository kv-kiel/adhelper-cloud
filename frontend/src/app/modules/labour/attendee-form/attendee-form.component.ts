import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IMember} from '../../../models/member';
import {IAttendee, IWorkHour} from '../../../models/labour';
import {LabourService} from '../labour.service';
import {MemberService} from '../../member/member.service';
import {fromEvent} from 'rxjs';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-attendee-form',
  templateUrl: './attendee-form.component.html',
  styleUrls: ['./attendee-form.component.scss']
})
export class AttendeeFormComponent implements OnInit {
  @Input() attendee: IAttendee | undefined = undefined;
  @Input() date: string = '';
  @Output() resultEvent = new EventEmitter<boolean>();

  notifyError: boolean = false;
  errorMessage: string = '';
  notifyInfo: boolean = false;
  infoMessage: string = '';
  loading: boolean = false;
  submitted: boolean = false;

  form!: FormGroup;
  members: IMember[] | undefined;
  nameFilter: string = '';
  selectedMemberId: number | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private memberService: MemberService,
    private labourService: LabourService
  ) { }

  ngOnInit(): void {
    this.requestMembers();

    this.form = this.formBuilder.group({
      date: [{value: '', disabled: true}, Validators.required],
      member: ['', Validators.required],
      hours: ['', [Validators.required, Validators.min(0.25), Validators.max(10)]]
    });

    if (this.attendee !== undefined) {
      this.form.controls.member.setValue(
        this.attendee.member.memberId + ': ' + this.attendee.member.lastName + ', ' + this.attendee.member.firstName);
      this.form.controls.hours.setValue(this.attendee.hours);
    }

    this.form.controls.date.setValue(this.date);
  }

  keyup(event: Event): void {
    this.nameFilter = this.form.controls.member.value;
  }

  onSelect(): void {
    const selection: string = this.form.controls.member.value;
    let id: number | undefined = Number(selection.split(':', 1));

    this.selectedMemberId = undefined;
    this.form.controls.member.setErrors(null);
    this.submitted = false;

    if (isNaN(id) && this.members) {
      const filteredMembers = this.members.filter(m => this.memberNameFilter(m, this.nameFilter));
      if (filteredMembers.length === 1) {
        const member: IMember = filteredMembers[0];
        this.form.controls.member.setValue(member.memberId + ': ' + member.lastName + ', ' + member.firstName);
        id = member.memberId;
      }
    }

    if (id && !isNaN(id)) {
      this.selectedMemberId = id;
      this.requestMemberHours();
    }
  }

  submit(): void {
    this.submitted = true;

    if (this.form.invalid || this.selectedMemberId === undefined) {
      return;
    }

    const workHour: IWorkHour = {
      date: this.form.controls.date.value,
      memberId: this.selectedMemberId,
      hours: this.form.controls.hours.value,
    };

    this.loading = true;
    this.labourService.create(workHour).subscribe(
      next => this.close(true),
      error => {
        this.loading = false;
        this.notifyError = true;
        this.errorMessage = error.message;
        fromEvent(document, 'click')
          .pipe(take(1))
          .subscribe(() => this.notifyError = false);
      }
    );
  }

  close(result: boolean): void {
    this.resultEvent.emit(result);
  }

  private memberNameFilter(m: IMember, filter: string): boolean | undefined {
    return m.lastName?.toLowerCase().includes(filter.toLowerCase()) ||
      m.firstName?.toLowerCase().includes(filter.toLowerCase());
  }

  private requestMembers(): void {
    this.memberService.getAll()
      .subscribe( next => {
        if (next && next.data) {
          this.members = next.data;
        }
      });
  }

  private requestMemberHours(): void {
    if (this.selectedMemberId === undefined) {
      return;
    }
    this.labourService.getMemberHours(this.selectedMemberId, new Map<string, string>([['fromDate', this.date], ['toDate', this.date]]))
      .subscribe(next => {
        if (next && next.length > 0) {
          this.form.controls.hours.setValue(next[0].hours);
          this.form.controls.member.setErrors({conflict: true});
          this.notifyInfo = true;
          this.infoMessage = 'Für das Mitglied wurden schon Stunden an diesem Datum verbucht!';
          fromEvent(document, 'click')
            .pipe(take(1))
            .subscribe(() => this.notifyInfo = false);
        }
      });
  }
}
