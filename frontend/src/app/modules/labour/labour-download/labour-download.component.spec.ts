import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabourDownloadComponent } from './labour-download.component';

describe('LabourDownloadComponent', () => {
  let component: LabourDownloadComponent;
  let fixture: ComponentFixture<LabourDownloadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabourDownloadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabourDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
