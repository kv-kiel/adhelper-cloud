import { Component, OnInit } from '@angular/core';
import {LabourService} from '../labour.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {IAttendee} from '../../../models/labour';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-labour-download',
  templateUrl: './labour-download.component.html',
  styleUrls: ['./labour-download.component.scss']
})
export class LabourDownloadComponent implements OnInit {

  constructor(
    private labourService: LabourService,
    private formBuilder: FormBuilder
  ) {
  }
  form!: FormGroup;

  ngOnInit(): void {
    const now: Date = new Date();
    const year: number = now.getFullYear();
    const from: string = year + '-01-01';
    const to: string = now.toISOString().substring(0, 10);
    this.form = this.formBuilder.group({
      fromDate: [from],
      toDate: [to],
    });
  }

  onSubmit(): void {
    const from = this.form.controls.fromDate.value;
    const to = this.form.controls.toDate.value;
    this.labourService.getAttendees(new Map([['fromDate', from], ['toDate', to]]))
      .pipe(
        map (next => next === undefined ? new Array<IAttendee>() : next),
        map(this.toCsv),
        map(next =>  new Blob([next], { type: 'text/csv;charset=utf-8;' }))
      ).subscribe(next => this.save(next, 'Arbeitsdienste.csv'));
  }

  toCsv = (attendees: IAttendee[]): string => {
    const d = ';';
    const data = attendees.map(a => {
      const date: Date = new Date(a.date);
      const dateString = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
      return a.member.memberId.toString() + d
        + a.member.lastName + ', '
        + a.member.firstName + d
        + dateString + d
        + a.hours.toFixed(2).replace('.', ',');
    });
    return [
      'Mitglieds_Nr;Nachname;Datum;Gel.Stunden',
      ...data
    ].join('\r\n');
  }

  save = (blob: Blob, fileName: string) => {
    if (navigator.msSaveBlob !== undefined) {
      navigator.msSaveBlob(blob, fileName);
    } else {
      const a = document.createElement('a');
      const url = window.URL.createObjectURL(blob);

      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    }
  }
}
