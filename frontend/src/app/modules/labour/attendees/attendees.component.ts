import {Component, OnInit} from '@angular/core';
import {IAttendee, IWorkHour} from '../../../models/labour';
import {LabourService} from '../labour.service';
import {DialogService} from '../../modal/dialog.service';
import {filter, take} from 'rxjs/operators';

@Component({
  selector: 'app-attendees',
  templateUrl: './attendees.component.html',
  styleUrls: ['./attendees.component.scss']
})
export class AttendeesComponent implements OnInit {
  attendees: IAttendee[] = new Array<IAttendee>();
  date: string = new Date().toISOString().substring(0, 10);


  constructor(
    private labourService: LabourService,
    private dialogService: DialogService
  ) {
  }

  ngOnInit(): void {
    this.loadAttendees();
  }

  onEdit(attendee?: IAttendee): void {
    const date: string = attendee
      ? attendee.date.toString().substring(0, 10)
        // ? attendee.date.getFullYear() + '-' + (attendee.date.getMonth() + 1) + '-' + attendee.date.getDate()
      : this.date;
    this.dialogService.showAttendeeDialog(date, attendee).pipe(
      take(1),
      filter(next => next === true),
    ).subscribe(() => {
      this.loadAttendees();
    });
  }

  onDelete(attendee: IAttendee): void {
    this.dialogService.showDialog(
      `Stunden für ${attendee.member.firstName} ${attendee.member.lastName} löschen?`,
      `Sollen die Stunden am wirklich gelöscht werden? Dies kann nicht rückgängig gemacht werden!`,
      'Löschen',
      'Abbrechen'
    ).pipe(
      take(1),
      filter(result => result === 'confirm')
    ).subscribe(
      () => this.labourService.deleteWorkHour(attendee).subscribe(() => this.loadAttendees())
    );

  }

  onChangeDate(event: Event): void {
    // @ts-ignore
    if (event.target && event.target.value) {
      // @ts-ignore
      this.date = event.target.value;
      this.loadAttendees();
    }
  }

  private loadAttendees(): void {
    this.labourService.getAttendees(new Map([['fromDate', this.date], ['toDate', this.date]])).subscribe(next => {
      if (next) {
        this.attendees = next;
      }
    });
  }
}
