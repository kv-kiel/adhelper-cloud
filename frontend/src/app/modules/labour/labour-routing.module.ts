import {NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LabourComponent} from './labour.component';
import {AttendeesComponent} from './attendees/attendees.component';
import {LabourDownloadComponent} from './labour-download/labour-download.component';
import {AuthenticationGuard} from '../../core/guards/authentication.guard';
import {AuthorizationGuard} from '../../core/guards/authorization.guard';
import {UserRole} from '../../models/user';


const routes: Routes = [
  { path: '',
    component: LabourComponent,
    children: [
      {path: '', component: AttendeesComponent,
        canActivate: [AuthorizationGuard],
        data: {roles: [UserRole.Admin, UserRole.Typist]}},
      {path: 'download', component: LabourDownloadComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabourRoutingModule {
}
