import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabourComponent } from './labour.component';
import {LabourRoutingModule} from './labour-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AttendeesComponent } from './attendees/attendees.component';
import { AttendeeFormComponent } from './attendee-form/attendee-form.component';
import { LabourDownloadComponent } from './labour-download/labour-download.component';


@NgModule({
  declarations: [LabourComponent, AttendeesComponent, AttendeeFormComponent, LabourDownloadComponent],
    imports: [
        CommonModule,
        LabourRoutingModule,
        ReactiveFormsModule,
        FormsModule,
    ]
})
export class LabourModule { }
