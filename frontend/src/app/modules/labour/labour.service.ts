import { Injectable } from '@angular/core';
import {RestService} from '../../core/services/rest.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IAttendee, IWorkHour} from '../../models/labour';
import {IUser} from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class LabourService {
  private readonly BASE_URL: string = '/labour';

  constructor(
    private rest: RestService
  ) {
  }

  public create(workHour: IWorkHour): Observable<void> {
    return this.rest.post(this.BASE_URL, workHour).pipe(
      map(res => {
        if (res.status !== 201) {
          throw new Error(res.statusText);
        }
        return;
      })
    );
  }

  public patchWorkHour(id: number, workHour: IWorkHour): Observable<void> {
    return this.rest.patch(this.BASE_URL + '/' + id, workHour).pipe(
      map( res => {
        switch (res.status) {
          case 204:
            return;
            break;
          case 404:
            throw new Error('Der Benutzer wurde nicht gefunden');
            break;
          default:
            throw new Error(`Unbekannter Fehler auf dem Server (${res.status})`);
            break;
        }
      })
    );
  }

  public getAll(params?: Map<string, string>): Observable<IWorkHour[] | undefined> {
    return this.rest.get<IWorkHour[]>(this.BASE_URL, params).pipe(
      map(res => res.body ?? undefined),
    );
  }

  public getAttendees(params?: Map<string, string>): Observable<IAttendee[] | undefined> {
    return this.rest.get<IAttendee[]>(this.BASE_URL + '/attendees', params).pipe(
      map(res => res.body ?? undefined),
    );
  }

  public getMemberHours(id: number, params?: Map<string, string>): Observable<IWorkHour[] | undefined> {
    return this.rest.get<IWorkHour[]>(this.BASE_URL + '/member/' + id, params).pipe(
      map(res => res.body ?? undefined),
    );
  }

  public deleteWorkHour(workHour: IWorkHour): Observable<void> {
    const url = this.BASE_URL + '/' + workHour.id;
    return this.rest.delete(url).pipe(
      map( res => {
        switch (res.status) {
          case 204:
            return;
            break;
          case 404:
            throw new Error('Der Benutzer wurde nicht gefunden');
            break;
          default:
            throw new Error(`Unbekannter Fehler auf dem Server (${res.status})`);
            break;
        }
      })
    );
  }
}
