import {Component, EventEmitter, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MemberService} from '../member.service';
import {fromEvent, interval} from 'rxjs';
import {take} from 'rxjs/operators';
import {IParseResult} from '../../../models/parse-result';

@Component({
  selector: 'app-data-upload',
  templateUrl: './member-upload.component.html',
  styleUrls: ['./member-upload.component.scss']
})
export class MemberUploadComponent {
  @Output() resultEvent = new EventEmitter<boolean>();

  form!: FormGroup;
  file: File | undefined;

  notifySuccess: boolean = false;
  notifyError: boolean = false;
  errorMessage: string = '';
  successMessage: string = '';

  private dataUploaded: boolean = false;

  constructor(
    private service: MemberService
  ) {
  }

  submit(): void {
    if (this.file !== undefined) {
      this.service.upload(this.file).subscribe(
        (next: IParseResult) => {
            this.dataUploaded = true;
            this.successMessage = `Es wurde ${next.rowCount} Datensätze gelesen davon ${next.invalidCount}` +
              ` fehlerhaft. Die Daten werden jetzt in die Datenbank geschrieben`;
            this.notifySuccess = true;
            interval(5000)
              .pipe(take(1))
              .subscribe(() => this.close());
        },
        error => {
          this.dataUploaded = false;
          this.errorMessage = error.message;
          this.notifyError = true;
          fromEvent(document, 'click')
            .pipe(take(1))
            .subscribe(() => this.close());
        }
      );
    }
  }

  onFileChange(event: Event): void {
    // @ts-ignore
    const files: File[] | undefined = event.target?.files ?? undefined;

    if (files !== undefined && files.length > 0) {
      this.file = files[0];
    }
  }

  close(): void {
    this.resultEvent.emit(this.dataUploaded);
  }
}
