import {NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MemberComponent} from './member.component';
import {MemberListComponent} from './member-list/member-list.component';


const routes: Routes = [
  { path: '',
    component: MemberComponent,
    children: [
      {path: '', component: MemberListComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule {
}
