import {TestBed} from '@angular/core/testing';
import {MemberService} from './member.service';
import {RestService} from '../../core/services/rest.service';
import {of} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {IParseResult} from '../../models/parse-result';

describe('MemberService', () => {
  let service: MemberService;
  let restServiceMock: { post: jasmine.Spy };

  const baseUrl = '/member';
  const uploadUrl = '/upload';

  // tslint:disable-next-line:no-any
  const file: any = {
    lastModified: 0,
    name: '',
    size: 0,
    type: ''
  };

  const parseResult: IParseResult = {rowCount: 0, invalidCount: 0, invalidRows: []};

  beforeEach(() => {
    const restServiceSpy = jasmine.createSpyObj('RestService', ['post']);

    TestBed.configureTestingModule({
      providers: [
        MemberService,
        {provide: RestService, useValue: restServiceSpy}
      ]
    });
    service = TestBed.inject(MemberService);
    restServiceMock = TestBed.inject(RestService) as jasmine.SpyObj<RestService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('upload', () => {
    it('should make post request and return ParseResult', () => {
      restServiceMock.post.and.returnValue(of(new HttpResponse<IParseResult>({body: parseResult, status: 200})));
      service.upload(file).subscribe(
        next => expect(next).toBe(parseResult),
        fail
      );
      expect(restServiceMock.post.calls.count()).toBe(1);
    });

    it('should throw error on empty response', () => {
      restServiceMock.post.and.returnValue(of(new HttpResponse<IParseResult>({status: 204})));
      service.upload(file).subscribe(
        next => fail('Expected error'),
        error => expect(error.message).toBe('')
      );
    });

    it('should throw error on error response', () => {
      restServiceMock.post.and.returnValue(of(new HttpResponse<IParseResult>({body: parseResult, status: 400})));
      service.upload(file).subscribe(
        next => fail('Expected error'),
        error => expect(error.message).toBe('')
      );
    });
  });
});
