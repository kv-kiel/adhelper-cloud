import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MemberComponent} from './member.component';
import {MemberRoutingModule} from './member-routing.module';
import {MemberUploadComponent } from './member-upload/member-upload.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MemberListComponent} from './member-list/member-list.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';


@NgModule({
  declarations: [MemberComponent, MemberUploadComponent, MemberListComponent],
    imports: [
        CommonModule,
        MemberRoutingModule,
        ReactiveFormsModule,
        InfiniteScrollModule
    ]
})
export class MemberModule {
}
