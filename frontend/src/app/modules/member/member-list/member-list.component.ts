import {Component, OnInit} from '@angular/core';
import {IMember} from '../../../models/member';
import {MemberService} from '../member.service';
import {DialogService} from '../../modal/dialog.service';
import {filter, take} from 'rxjs/operators';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss']
})
export class MemberListComponent implements OnInit {
  infiniteScrollFilter: Map<string, string> = new Map<string, string>();
  from: number = 0;
  readonly TAKE: number = 50;
  members: IMember[] = new Array<IMember>();
  total: number = 0;

  constructor(
    private service: MemberService,
    private dialogService: DialogService
  ) {
  }

  ngOnInit(): void {
    this.infiniteScrollFilter.set('take', this.TAKE.toString());
    this.load();
  }

  private load(): void {
    this.infiniteScrollFilter.set('from', this.from.toString());
    this.service.getAll(this.infiniteScrollFilter).subscribe(next => {
      if (next) {
        this.members = this.members.concat(next.data);
        this.total = next.total;
        this.from = next.from + next.take;
      }
    });
  }

  onScroll(): void {
    this.load();
  }

  onUploadButton(): void {
    this.dialogService.showUploadDialog().pipe(
      take(1),
      filter(next => next === true),
    ).subscribe(() => {
      this.from = 0;
      this.members = new Array<IMember>();
      this.load();
    });
  }
}
