import {Injectable} from '@angular/core';
import {RestService} from '../../core/services/rest.service';
import {Observable} from 'rxjs';
import {IParseResult} from '../../models/parse-result';
import {map} from 'rxjs/operators';
import {IMember} from '../../models/member';
import {IListResponse} from '../../models/list-response';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  private readonly BASE_URL: string = '/member';

  constructor(
    private rest: RestService
  ) {
  }

  public upload(file: File): Observable<IParseResult> {
    const formData: FormData = new FormData();
    formData.append('baseData', file);
    return this.rest.post<IParseResult>(this.BASE_URL + '/upload', formData).pipe(
      map(res => {
        if (res.body && res.status === 200) {
          return res.body;
        }
        throw new Error();
      })
    );
  }

  public getAll(params?: Map<string, string>): Observable<IListResponse<IMember> | undefined> {
    return this.rest.get<IListResponse<IMember>>(this.BASE_URL, params).pipe(
      map(res => res.body ?? undefined)
    );
  }

  public find(params: Map<string, string>): Observable<IListResponse<IMember> | undefined> {
    return this.rest.get<IListResponse<IMember>>(this.BASE_URL + '/find', params).pipe(
      map(res => res.body ?? undefined)
    );
  }

}
