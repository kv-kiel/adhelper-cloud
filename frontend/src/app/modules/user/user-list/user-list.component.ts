import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {IUser} from '../../../models/user';
import {DialogService} from '../../modal/dialog.service';
import {filter, take} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users: IUser[] | null = null;

  constructor(
    private service: UserService,
    private dialogService: DialogService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.load();
  }

  onDelete(user: IUser): void {
    this.dialogService.showDialog(
      'Benutzer löschen?',
      `Soll ${user.username} (${user.id}) wirklich gelöscht werden? Dies kann nicht rückgängig gemacht werden!`,
      'Löschen',
      'Abbrechen'
    ).pipe(
      take(1),
      filter(result => result === 'confirm')
    ).subscribe(
      () => this.service.deleteUser(user).subscribe(() => this.load())
    );
  }

  private load(): void {
    this.service.getAll().subscribe(users => this.users = users);
  }

}
