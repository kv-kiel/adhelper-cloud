import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IUser} from '../../models/user';
import {UserService} from './user.service';
import {Observable} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserResolver implements Resolve<IUser> {
  constructor(private service: UserService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUser> {
    const id = route.paramMap.get('id');

    if (id === null) {
      throw new Error();
    }

    return this.service.getUser(id);
  }
}
