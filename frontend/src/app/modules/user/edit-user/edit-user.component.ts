import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IUser} from '../../../models/user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  public user!: IUser;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.user = this.activatedRoute.snapshot.data.user;
  }

  onSubmit(editedUser: IUser): void {
    if (this.user.id !== undefined) {
      this.userService.patchUser(this.user.id.toString(), editedUser).subscribe();
    }
  }
}
