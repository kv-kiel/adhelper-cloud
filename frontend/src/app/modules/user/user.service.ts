import {Injectable} from '@angular/core';
import {RestService} from '../../core/services/rest.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IUser} from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly BASE_URL: string = '/user';

  constructor(
    private rest: RestService
  ) {
  }

  public getAll(): Observable<IUser[]> {
    return this.rest.get<IUser[]>(this.BASE_URL).pipe(
      map(res => res.body ?? new Array<IUser>())
    );
  }

  public getUser(id: string): Observable<IUser> {
    return this.rest.get<IUser>(this.BASE_URL + '/' + id).pipe(
      map(res => {
        if (res.body) {
          return res.body;
        }
        throw new Error();
      })
    );
  }

  public createUser(user: IUser): Observable<void> {
    return this.rest.post(this.BASE_URL, user).pipe(
      map(res => {
        if (res.status !== 201) {
          throw new Error(res.statusText);
        }
        return;
      })
    );
  }

  public deleteUser(user: IUser): Observable<void> {
    const url = this.BASE_URL + '/' + user.id;
    return this.rest.delete(url).pipe(
      map( res => {
        switch (res.status) {
          case 204:
            return;
            break;
          case 404:
            throw new Error('Der Benutzer wurde nicht gefunden');
            break;
          default:
            throw new Error(`Unbekannter Fehler auf dem Server (${res.status})`);
            break;
        }
      })
    );
  }

  public patchUser(id: string, user: IUser): Observable<void> {
    const url = this.BASE_URL + '/' + id;
    return this.rest.patch(url, user).pipe(
      map( res => {
        switch (res.status) {
          case 204:
            return;
            break;
          case 404:
            throw new Error('Der Benutzer wurde nicht gefunden');
            break;
          default:
            throw new Error(`Unbekannter Fehler auf dem Server (${res.status})`);
            break;
        }
      })
    );
  }
}
