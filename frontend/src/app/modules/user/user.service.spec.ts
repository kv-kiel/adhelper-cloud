import {TestBed} from '@angular/core/testing';

import {UserService} from './user.service';
import {RestService} from '../../core/services/rest.service';
import {EMPTY, of} from 'rxjs';
import {IUser, UserRole} from '../../models/user';
import {HttpResponse} from '@angular/common/http';

describe('UserService', () => {
  let service: UserService;
  let restServiceMock: {get: jasmine.Spy, post: jasmine.Spy, delete: jasmine.Spy};

  const user: IUser = {
    email: 'test@mail.ee',
    lastName: 'Test',
    firstName: 'Test',
    username: 'Test',
    role: UserRole.User,
    id: 1,
  };

  const userUrl = '/user';
  const createUrl = '/user';
  const deleteUrl = '/user/';

  beforeEach(() => {
    const restServiceSpy = jasmine.createSpyObj('RestService', ['get', 'post', 'delete']);

    TestBed.configureTestingModule({
      providers: [
        UserService,
        {provide: RestService, useValue: restServiceSpy}
      ]
    });
    service = TestBed.inject(UserService);
    restServiceMock = TestBed.inject(RestService) as jasmine.SpyObj<RestService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make a get request with correct parameters', () => {
    restServiceMock.get.and.returnValue(EMPTY);
    service.getAll().subscribe(
      next => expect(next).toBeDefined(),
      error => fail
    );
    expect(restServiceMock.get)
      .toHaveBeenCalledWith(userUrl);
    expect(restServiceMock.get.calls.count()).toBe(1);
  });

  it('should return an array of Users', () => {
    const user2: IUser = {...user, id: 2};
    const users: IUser[] = [user, user2];

    restServiceMock.get.and.returnValue(of(new HttpResponse<IUser[]>({body: users})));

    service.getAll().subscribe(
      next => expect(next).toEqual(users),
      error => fail
    );
  });

  it('it should return Empty Array on Error', () => {
    restServiceMock.get.and.returnValue(of(new HttpResponse({status: 500})));

    service.getAll().subscribe(
      next => expect(next).toBeDefined(),
      error => fail
    );
  });

  describe('createUser', () => {
    it('should post with correct parameters', () => {
      restServiceMock.post.and.returnValue(of(new HttpResponse({status: 204})));
      service.createUser(user).subscribe(next => {}, error => fail);
      expect(restServiceMock.post).toHaveBeenCalledWith(createUrl, user);
      expect(restServiceMock.post.calls.count()).toBe(1);
    });
  });

  describe('deleteUser', () => {
    it('should send method delete to url with user id', () => {
      restServiceMock.delete.and.returnValue(EMPTY);
      service.deleteUser(user).subscribe(() => {}, () => fail);
      expect(restServiceMock.delete).toHaveBeenCalledWith(deleteUrl + user.id);
      expect(restServiceMock.delete.calls.count()).toBe(1);
    });

    it('should return void Observable on Status 204', () => {
      restServiceMock.delete.and.returnValue(of(new HttpResponse({status: 204})));
      service.deleteUser(user).subscribe(
        value => expect(value).toBe(),
        error => fail
      );
    });

    it('should throw specific error on Status 404', () => {
      restServiceMock.delete.and.returnValue(of(new HttpResponse({status: 404})));
      service.deleteUser(user).subscribe(
        next => fail('Expected error'),
        error => expect(error.message).toContain('Der Benutzer wurde nicht gefunden')
      );
    });

    it('should throw error with status code in message', () => {
      restServiceMock.delete.and.returnValue(of(new HttpResponse({status: 500})));
      service.deleteUser(user).subscribe(
        next => fail('Expected error'),
        error => expect(error.message).toBe('Unbekannter Fehler auf dem Server (500)')
      );
    });
  });
});
