import {NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from './user.component';
import {UserListComponent} from './user-list/user-list.component';
import {CreateUserComponent} from './create-user/create-user.component';
import {EditUserComponent} from './edit-user/edit-user.component';
import {UserResolver} from './user.resolver';


const routes: Routes = [
  { path: '',
    component: UserComponent,
    children: [
      {path: '', component: UserListComponent},
      {path: 'create', component: CreateUserComponent},
      {path: ':id/edit', component: EditUserComponent, resolve: {user: UserResolver}},
      {path: ':id', redirectTo: '', pathMatch: 'full'},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
