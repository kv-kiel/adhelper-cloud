import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserListComponent} from './user-list/user-list.component';
import {UserComponent} from './user.component';
import {UserRoutingModule} from './user-routing.module';
import {UserFormComponent} from './user-form/user-form.component';
import {CreateUserComponent} from './create-user/create-user.component';
import {ReactiveFormsModule} from '@angular/forms';
import {EditUserComponent} from './edit-user/edit-user.component';

@NgModule({
  declarations: [UserListComponent, UserComponent, UserFormComponent, CreateUserComponent, EditUserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule
  ]
})
export class UserModule {
}
