import { Component, OnInit } from '@angular/core';
import {IUser} from '../../../models/user';
import {UserService} from '../user.service';
import {fromEvent, interval} from 'rxjs';
import {take} from 'rxjs/operators';
import {Location} from '@angular/common';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  notifySuccess: boolean = false;
  notifyError: boolean = false;
  errorMessage: string = '';

  constructor(
    private userService: UserService,
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  submit(user: IUser): void {
    this.userService.createUser(user)
      .subscribe(
        () => {
          this.notifySuccess = true;
          interval(1000)
            .pipe(take(1))
            .subscribe(() => this.location.back());
        },
        error => {
          this.errorMessage = error.error;
          this.notifyError = true;
          fromEvent(document, 'click')
            .pipe(take(1))
            .subscribe(() => this.notifyError = false);
        }
      );
  }
}
