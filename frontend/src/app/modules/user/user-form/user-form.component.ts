import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IUser, UserRole} from '../../../models/user';
import {fieldMatchValidator} from '../../../shared/field-match.validator';


@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  formGroup!: FormGroup;
  profileFormGroup!: FormGroup;
  permissionsFormGroup?: FormGroup;
  passwordFormGroup?: FormGroup;

  submitted = false;

  userRole = UserRole;

  @Input() user?: IUser;
  @Input() showPasswordGroup: boolean = false;
  @Input() showPermissionsGroup: boolean = false;
  @Output() submitEvent = new EventEmitter<IUser>();

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.profileFormGroup = this.formBuilder.group({
      username: [this.user?.username ?? '', Validators.required],
      firstName: [this.user?.firstName ?? '', Validators.required],
      lastName: [this.user?.lastName ?? '', Validators.required],
      email: [this.user?.email ?? '', [Validators.required, Validators.email]],
      memberId: [{value: this.user?.memberId ?? '', disabled: this.user?.memberIdApproved ?? false}]
      // memberId: [this.user?.memberId ?? '']
    });

    this.formGroup = new FormGroup({
      profile: this.profileFormGroup,
    });

    if (this.showPasswordGroup === true) {
      this.passwordFormGroup = this.formBuilder.group({
        newPassword: ['', Validators.required],
        repeat: ['', Validators.required]
      }, {
        validators: [fieldMatchValidator('newPassword', 'repeat')]
      });
      this.formGroup.addControl('password', this.passwordFormGroup);
    }

    if (this.showPermissionsGroup === true) {
      this.permissionsFormGroup = this.formBuilder.group({
        role: [this.user?.role ?? UserRole.User, Validators.required],
        memberIdApproved: [this.user?.memberIdApproved ?? false],
        enabled: [this.user?.enabled ?? false],
      });
      this.formGroup.addControl('permissions', this.permissionsFormGroup);
    }
  }

  submit(): void {
    this.submitted = true;

    if (this.formGroup.invalid) {
      return;
    }

    this.user = {};

    this.user.username =
      this.profileFormGroup.controls.username.dirty
        ? this.profileFormGroup.controls.username.value
        : undefined;
    this.user.email =
      this.profileFormGroup.controls.email.dirty
        ? this.profileFormGroup.controls.email.value
        : undefined;
    this.user.firstName =
      this.profileFormGroup.controls.firstName.dirty
        ? this.profileFormGroup.controls.firstName.value
        : undefined;
    this.user.lastName =
      this.profileFormGroup.controls.lastName.dirty
        ? this.profileFormGroup.controls.lastName.value
        : undefined;
    this.user.memberId =
      this.profileFormGroup.controls.memberId.dirty && this.profileFormGroup.controls.memberId.value.length > 0
        ? this.profileFormGroup.controls.memberId.value
        : undefined;

    if (this.permissionsFormGroup) {
      this.user.enabled =
        this.permissionsFormGroup.controls.enabled.dirty
          ? this.permissionsFormGroup.controls.enabled.value
          : undefined;
      this.user.memberIdApproved =
        this.permissionsFormGroup.controls.memberIdApproved.dirty
          ? this.permissionsFormGroup.controls.memberIdApproved.value
          : undefined;
      this.user.role =
        this.permissionsFormGroup.controls.role.dirty
          ? this.permissionsFormGroup.controls.role.value as UserRole
          : undefined;
    }

    if (this.passwordFormGroup) {
      this.user.newPassword =
        this.passwordFormGroup.controls.newPassword.value
          ? this.passwordFormGroup.controls.newPassword.value
          : undefined;
    }

    this.submitEvent.emit(this.user);
  }
}

