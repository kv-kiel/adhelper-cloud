import {FormGroup} from '@angular/forms';


// custom validator to check that two fields match
export function fieldMatchValidator(controlName: string, matchingControlName: string):
  (formGroup: FormGroup) => { mustMatch: boolean } | null {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({mustMatch: true});
      return {mustMatch: true};
    } else {
      matchingControl.setErrors(null);
      return null;
    }
  };
}
