import {Component, OnInit} from '@angular/core';
import {SessionService} from './core/services/session.service';
import {UserRole} from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'AD-Helper-Cloud';
  loggedIn = false;
  role: UserRole | undefined = undefined;
  readonly ROLE = UserRole;
  username: string | undefined = undefined;

  constructor(
    private sessionService: SessionService
  ) {
  }

  ngOnInit(): void {
    this.sessionService.getLoggedIn().subscribe((status: boolean) => {
      this.loggedIn = status;
      if (status) {
        this.role = this.sessionService.getRole();
        this.username = this.sessionService.getUsername();
      }
    });
  }

  logout(): void {
    this.sessionService.logout();
  }
}
