import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SettingService} from '../../core/services/setting.service';
import {fromEvent} from 'rxjs';
import {take} from 'rxjs/operators';
import {fieldMatchValidator} from '../../shared/field-match.validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  form!: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  notifySuccess: boolean = false;
  notifyError: boolean = false;
  errorMessage: string = '';
  readonly minLength: number = 5;

  constructor(
    private formBuilder: FormBuilder,
    private userService: SettingService
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(this.minLength)]],
      repeatPassword: ['', Validators.required]
    }, {
      validators: [fieldMatchValidator('newPassword', 'repeatPassword')]
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.userService.changePassword(this.form.controls.oldPassword.value, this.form.controls.newPassword.value).subscribe(
      success => {
        this.loading = false;
        if (success) {
          this.notifySuccess = true;
          fromEvent(document, 'click')
            .pipe(take(1))
            .subscribe(() => this.notifySuccess = false);
        } else {
          throw new Error();
        }
      },
      err => {
        console.log(err);
        this.loading = false;
        this.notifyError = true;
        if (err.status === 403) {
          this.errorMessage = 'Das alte Passwort ist nicht korrekt';
        }
        fromEvent(document, 'click')
          .pipe(take(1))
          .subscribe(() => this.notifyError = false);
      }
    );
  }
}
