import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SessionService} from '../../core/services/session.service';
import {Router} from '@angular/router';
import {take} from 'rxjs/operators';
import {fromEvent} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  loading = false;
  submitted = false;
  notifyError = false;
  notification: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: SessionService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  submit(): void{
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.authenticationService.login(this.form.controls.username.value, this.form.controls.password.value)
      .subscribe(
        success => {
          if (success) {
            this.router.navigate(['/home']);
          }
        },
        error => {
          this.notifyError = true;
          if (error.status  === 401) {
            this.notification = 'Benutzername oder Passwort sind leider falsch';
          } else {
            this.notification = `Es ist ein Problem auf dem Server aufgetreten (${error.status})`;
          }
          console.log(error.status);
          fromEvent(document, 'click')
            .pipe(take(1))
            .subscribe(() => this.notifyError = false);
        }
      );
  }
}
