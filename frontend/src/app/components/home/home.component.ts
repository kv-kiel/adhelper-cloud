import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../core/services/session.service';
import {UserRole} from '../../models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private sessionService: SessionService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    switch (this.sessionService.getRole()) {
      case UserRole.Admin:
        this.router.navigate(['/user']);
        break;
      case UserRole.Assessor:
        this.router.navigate(['/member']);
        break;
      default:
        break;
    }
  }

}
