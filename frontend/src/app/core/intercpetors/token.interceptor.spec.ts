import {TestBed} from '@angular/core/testing';
import {TokenInterceptor} from './token.interceptor';
import {SessionService} from '../services/session.service';

xdescribe('TokenInterceptor', () => {
  let sessionServiceSpy: { getToken: jasmine.Spy };

  beforeEach(() => {
    const spy = jasmine.createSpyObj('SessionService', ['getToken']);
    TestBed.configureTestingModule({
      providers: [
        TokenInterceptor,
        {provide: SessionService, useValue: spy}
      ]
    });
    sessionServiceSpy = TestBed.inject(SessionService) as jasmine.SpyObj<SessionService>;
  });


  it('should be created', () => {
    const interceptor: TokenInterceptor = TestBed.inject(TokenInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
