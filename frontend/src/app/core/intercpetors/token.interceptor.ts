import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SessionService} from '../services/session.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private sessionService: SessionService
  ) {
  }

  /**
   * The TokenInterceptor includes the bearer token in the authorization header of all
   * requests to our api url as long as we are logged in.
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (!this.sessionService.getToken() || !request.url.startsWith(environment.apiUrl)) {
      return next.handle(request);
    }

    const token = this.sessionService.getToken();

    const authReq = request.clone({ setHeaders: {Authorization: 'Bearer ' + token}});

    return next.handle(authReq);
  }
}
