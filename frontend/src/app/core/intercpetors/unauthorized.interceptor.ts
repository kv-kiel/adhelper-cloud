import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {SessionService} from '../services/session.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class UnauthorizedInterceptor implements HttpInterceptor {

  constructor(
    private sessionService: SessionService
  ) {
  }

  /**
   * The unauthorized interceptor performs an logout, if an un authorized error was returned on
   * an api request
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401 && this.sessionService.isLoggedIn()) {
        this.sessionService.logout();
        console.log(err);
      }

      return throwError(err);
    }));
  }
}
