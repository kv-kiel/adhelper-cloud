import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {SessionService} from '../services/session.service';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(
    private router: Router,
    private sessionService: SessionService
  ) {
  }

  /**
   * This guard redirects to the login component if the login status is false
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> {

    if (this.sessionService.isLoggedIn() === true) {
      return of(true);
    }

    return this.sessionService.checkSession().pipe(
      map(value => value === true ? true : this.router.parseUrl('/login'))
    );
  }
}
