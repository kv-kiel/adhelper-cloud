import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {SessionService} from '../services/session.service';
import {UserRole} from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {
  constructor(
    private sessionService: SessionService
  ) {
  }

  /**
   * This guard calls the sessionService to check, if one of the passed roles matches the role of the
   * user currently logged in.
   * The roles are passed in the routing module as data object in the route definition like
   * data: {roles: [UserRole.Admin, UserRole.User]}
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const roles: Array<UserRole> = route.data.roles as Array<UserRole> ?? new Array<UserRole>(UserRole.User);
    return this.sessionService.hasRole(roles);
  }
}
