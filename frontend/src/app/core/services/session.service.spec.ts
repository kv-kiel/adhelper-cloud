import {TestBed} from '@angular/core/testing';
import {SessionService} from './session.service';
import {RestService} from './rest.service';
import {EMPTY, of} from 'rxjs';
import {Router} from '@angular/router';
import * as jwt from 'jsonwebtoken';
import {HttpResponse} from '@angular/common/http';
import {ILoginResponse} from '../../models/login.response';
import {UserRole} from '../../models/user';

describe('SessionService', () => {
  let service: SessionService;
  let restServiceMock: { post: jasmine.Spy, get: jasmine.Spy };
  let routerSpy: { navigate: jasmine.Spy };

  const loginUrl = '/authentication/login';

  const username = 'blinky';
  const userRole = UserRole.User;
  const password = 'password';
  const expiresIn = '1h';

  const token = jwt.sign(
    {username, userRole},
    'secret',
    {expiresIn});
  const httpResponse = new HttpResponse<ILoginResponse>({status: 200, body: {token}});


  beforeEach(() => {
    const restSpy = jasmine.createSpyObj('RestService', ['post', 'get']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      providers: [
        SessionService,
        {provide: RestService, useValue: restSpy},
        {provide: Router, useValue: routerSpy}
      ]
    });
    service = TestBed.inject(SessionService);
    restServiceMock = TestBed.inject(RestService) as jasmine.SpyObj<RestService>;
    service = TestBed.inject(SessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('login', () => {


    it('should post to login api', () => {
      restServiceMock.post.and.returnValue(EMPTY);
      service.login(username, password).subscribe(() => {
      }, fail);
      expect(restServiceMock.post).toHaveBeenCalledWith(loginUrl, {username, password});
    });

    it('should return false on error', () => {
      restServiceMock.post.and.returnValue(of(new HttpResponse({status: 401})));
      service.login(username, password).subscribe(
        next => expect(next).toBeFalse(),
        fail);
      expect(restServiceMock.post).toHaveBeenCalledWith(loginUrl, {username, password});
    });

    describe('when successful', () => {
      beforeEach(() => {
        service.logout();
        restServiceMock.post.and.returnValue(of(httpResponse));
      });

      it('updates the LoggedIn state', () => {
        expect(service.isLoggedIn()).toBeFalse();
        service.login(username, password).subscribe(
          value => expect(value).toBeTrue(),
          fail
        );
        expect(service.isLoggedIn()).toBeTrue();
      });

      it('sets the session with role and username', () => {
        service.login(username, password).subscribe(() => {
        }, fail);
        expect(service.getUsername()).toBe(username);
        expect(service.getRole()).toBe(userRole);
      });

      it('sets the token', () => {
        service.login(username, password).subscribe(() => {
        }, fail);
        expect(service.getToken()).toBe(token);
        expect(localStorage.getItem('token')).toBe(token);
      });
    });
  });

  describe('logout', () => {
    beforeEach(() => {
      restServiceMock.post.and.returnValue(of(httpResponse));
      service.login(username, password).subscribe(next => {}, fail);
    });

    it('updates the state', () => {
      expect(service.isLoggedIn()).toBeTrue();
      service.logout();
      expect(service.isLoggedIn()).toBeFalse();
    });

    it('removes the token from memory and localStorage', () => {
      expect(service.getToken()).toBe(token);
      expect(localStorage.getItem('token')).toBe(token);
      service.logout();
      expect(service.getToken()).toBe(null);
      expect(localStorage.getItem('token')).toBe(null);
    });

    it('navigates to /login', () => {
      service.logout();
      expect(routerSpy.navigate).toHaveBeenCalledWith(['/login']);
    });
  });

  describe('checkSession', () => {
    it('returns false when no token is stored', () => {
      service.logout();
      service.checkSession().subscribe(
        next => expect(next).toBeFalse(),
        fail
      );
      expect(restServiceMock.get.calls.count()).toBe(0);
    });

    it('refreshes session if token was found', () => {
      service.logout();
      localStorage.setItem('token', token);
      restServiceMock.get.and.returnValue(of(httpResponse));
      service.checkSession().subscribe(
        next => expect(next).toBeTrue(),
        fail
      );
      expect(restServiceMock.get.calls.count()).toBe(1);
    });
  });
});
