import {TestBed} from '@angular/core/testing';

import {RestService} from './rest.service';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {environment} from '../../../environments/environment';

describe('RestService', () => {
  let service: RestService;
  let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };

  const url = '/testurl';

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpService', ['get', 'post']);

    TestBed.configureTestingModule({
      providers: [
        RestService,
        {provide: HttpClient, useValue: spy}
      ]
    });
    service = TestBed.inject(RestService);
    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make get requests with correct parameters', () => {
    httpClientSpy.get.and.returnValue(of(new HttpResponse<number>()));
    service.get<number>(url).subscribe(
      response => expect(response).toBeInstanceOf(HttpResponse)
    );
    expect(httpClientSpy.get)
      .toHaveBeenCalledWith(environment.apiUrl + url, Object({observe: 'response'}));
  });

  it('should make post requests with correct parameters', () => {
    const body = {value: 'test'};
    httpClientSpy.post.and.returnValue(of(new HttpResponse<number>()));
    service.post<number>(url, body).subscribe(
      response => expect(response).toBeInstanceOf(HttpResponse)
    );
    expect(httpClientSpy.post)
      .toHaveBeenCalledWith(environment.apiUrl + url, body, Object({observe: 'response'}));
  });
});
