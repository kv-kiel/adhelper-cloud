import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

/**
 * The rest service uses the angular HttpClient and supply rest methods which
 * already now the api base url and all return a HttpResponse<T> Observable
 */
@Injectable({
  providedIn: 'root'
})
export class RestService {

  private readonly apiUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.apiUrl = environment.apiUrl;
  }

  get<T>(url: string, params?: Map<string, string>): Observable<HttpResponse<T>> {
    url = this.apiUrl + url;
    if (params) {
      url += this.paramsToQuery(params);
    }
    return this.http.get<T>(url, {observe: 'response'});
  }

  post<T>(url: string, body: object): Observable<HttpResponse<T>> {
    url = this.apiUrl + url;
    return this.http.post<T>(url, body, {observe: 'response'});
  }

  patch<T>(url: string, body: object): Observable<HttpResponse<T>> {
    url = this.apiUrl + url;
    return this.http.patch<T>(url, body, {observe: 'response'});
  }

  delete(url: string): Observable<HttpResponse<unknown>> {
    url = this.apiUrl + url;
    return this.http.delete(url, {observe: 'response'});
  }

  private paramsToQuery(params: Map<string, string>): string {
    let query: string = '';
    params.forEach((k, v) => {
      query += '&' + v + '=' + k;
    });
    return query.replace('&', '?');
  }
}
