import {Injectable} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {BehaviorSubject, Observable, of, Subject, throwError, timer} from 'rxjs';
import {catchError, filter, map, take, takeLast, takeUntil, tap} from 'rxjs/operators';
import {RestService} from './rest.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {ISession} from '../../models/session';
import {ILoginResponse} from '../../models/login.response';
import {UserRole} from '../../models/user';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private readonly BASE_URL = '/authentication';
  private readonly TOKEN_NAME = 'token';

  /**
   * This timer counts down the session age and initiates logout on expiry
   */
  private sessionTimer$: Observable<number> | null = null;
  /**
   * By this subject the sessionTimer$ can be intercepted prior to it's expiry
   */
  private timerInterceptorSubject: Subject<null> = new Subject<null>();
  /**
   * This is the source of truth for actual session related data
   */
  private session: ISession | null = null;
  /**
   * Holds the current login status
   */
  private loggedInSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  /**
   * The current jwt received from the last login repsonse
   */
  private token: string | null = null;

  constructor(
    private rest: RestService,
    private router: Router
  ) {
  }

  /**
   * Perform a login by posting credentials to the data and handling the response with the session token
   * @param username the name of the username to be logged in
   * @param password the newPassword
   */
  public login(username: string, password: string): Observable<boolean> {

    return this.rest.post<ILoginResponse>(this.BASE_URL + '/login', {username, password})
      .pipe(
        map((res: HttpResponse<ILoginResponse>) => {
          if (res.body?.token !== undefined) {
            this.setSession(res.body.token);
            return true;
          }
          return false;
        })
      );
  }

  /**
   * Performs a logout by changing state, removing session data and deleting token from local storage
   */
  public logout(): void {
    this.timerInterceptorSubject.next();
    this.loggedInSubject.next(false);
    this.session = null;
    this.token = null;
    localStorage.removeItem(this.TOKEN_NAME);
    this.router.navigate(['/login']);
  }

  /**
   * Return the current login status as true or false
   */
  public isLoggedIn(): boolean {
    return this.loggedInSubject.getValue();
  }

  public checkSession(): Observable<boolean> {
    this.token = localStorage.getItem(this.TOKEN_NAME);

    if (this.token === null) {
      return of(false);
    }

    return this.rest.get<ILoginResponse>(this.BASE_URL + '/token')
      .pipe(
        map((res: HttpResponse<ILoginResponse>) => {
          if (res.body?.token !== undefined) {
            this.setSession(res.body.token);
          }
          return this.isLoggedIn();
        }),
        catchError(() => of(false))
      );
  }

  /**
   * Check if the user currently logged in has one of the given roles
   * @param roles - Array of roles the user shall be checked against
   */
  public hasRole(roles: UserRole[]): boolean {
    return this.session !== null && roles.includes(this.session.userRole);
  }

  /**
   * Get the role of the currently logged in user or null if not logged in
   */
  public getRole(): UserRole | undefined {
    return this.session?.userRole;
  }

  /**
   * Returns an observable which will emit the status of login as true or false
   */
  public getLoggedIn(): Observable<boolean> {
    return this.loggedInSubject.asObservable();
  }

  /**
   * Returns the username of the currently logged in user or an empty string
   */
  public getUsername(): string | undefined {
    return this.session?.username;
  }

  /**
   * Returns the current jwt token
   */
  public getToken(): string | null {
    return this.token;
  }

  /**
   * Store the jwt to the localStorage, decode the token, store it's data as session information
   * and initiate session timer.
   * @param token encoded jwt-string
   */
  private setSession(token: string): void {
    this.token = token;
    localStorage.setItem(this.TOKEN_NAME, token);
    const jwt = new JwtHelperService();
    const decoded = jwt.decodeToken(token);

    this.session = {
      username: decoded.username ?? '',
      userRole: decoded.userRole ?? UserRole.User,
      expiry: decoded.exp,
      maxAgeSeconds: decoded.exp - Math.floor(Date.now() / 1000)
    };

    this.loggedInSubject.next(true);

    this.sessionTimer$ = timer(0, 1000).pipe(
      takeUntil(this.timerInterceptorSubject),             // stop when interceptor fires
      take((this.session?.maxAgeSeconds ?? -1) + 1), // stop when max age is reached
      map((elapsed: number) => (this.session?.maxAgeSeconds ?? elapsed) - elapsed), // elapsed => remaining
    );

    this.sessionTimer$.pipe(
      takeLast(1),                        // fire only on the last tick ...
      filter((x: number) => x === 0)   // when the session expired (and not when it was intercepted)
    ).subscribe(() => this.logout());
  }
}
