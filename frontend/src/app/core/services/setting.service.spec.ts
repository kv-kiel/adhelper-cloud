import {TestBed} from '@angular/core/testing';

import {SettingService} from './setting.service';
import {RestService} from './rest.service';
import {EMPTY, of} from 'rxjs';
import {HttpResponse} from '@angular/common/http';

describe('SettingService', () => {
  let service: SettingService;
  let restServiceMock: { post: jasmine.Spy };

  beforeEach(() => {
    const restServiceSpy = jasmine.createSpyObj('RestService', ['post']);

    TestBed.configureTestingModule({
      providers: [
        SettingService,
        {provide: RestService, useValue: restServiceSpy}
      ]
    });
    service = TestBed.inject(SettingService);
    restServiceMock = TestBed.inject(RestService) as jasmine.SpyObj<RestService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('changePassword', () => {
    it('should post to /setting/newPassword', () => {
      restServiceMock.post.and.returnValue(EMPTY);
      service.changePassword('old', 'new').subscribe(
        () => {},
        fail
      );
      expect(restServiceMock.post).toHaveBeenCalledWith('/setting/password', {oldPassword: 'old', newPassword: 'new'});
      expect(restServiceMock.post.calls.count()).toBe(1);
    });

    it('should return true on status 204', () => {
      restServiceMock.post.and.returnValue(of(new HttpResponse({status: 204})));
      service.changePassword('old', 'new').subscribe(
        value => expect(value).toBeTrue(),
        fail
      );
    });

    it('should return false on other status', () => {
      restServiceMock.post.and.returnValue(of(new HttpResponse({status: 403})));
      service.changePassword('old', 'new').subscribe(
        value => expect(value).toBeFalse(),
        fail
      );
    });
  });
});
