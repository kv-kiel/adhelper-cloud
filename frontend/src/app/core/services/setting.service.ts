import { Injectable } from '@angular/core';
import {RestService} from './rest.service';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  private readonly BASE_URL: string = '/setting';
  private readonly CHANGE_PASSWORD_URL: string = this.BASE_URL + '/password';

  constructor(
    private rest: RestService
  ) { }

  public changePassword(oldPassword: string, newPassword: string): Observable<boolean> {
    return this.rest.post(this.CHANGE_PASSWORD_URL, {oldPassword, newPassword})
      .pipe(
        map((res: HttpResponse<unknown>) =>  res.status === 204)
      );
  }
}
