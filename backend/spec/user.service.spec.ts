import {UserService} from "../src/services/user.service";
import * as typeorm from "typeorm";
import * as sinon from "sinon";
import {of} from "rxjs";
import {IUser} from "../src/models/user";

describe('UserService', () => {
    let service: UserService;
    let repositoryMock: {find: jasmine.Spy};

    beforeAll(() => {
        repositoryMock = jasmine.createSpyObj('Repository', ['find']);
        sinon.replace(typeorm, 'getRepository', () => {return repositoryMock as any});
        service = new UserService();
    });

    afterAll(() => {
       sinon.restore();
    });

    describe('getAll', () => {
       it('should call find with select option', () => {
           repositoryMock.find.and.returnValue(of(new Array<IUser>()));
           service.getAll().subscribe(
               next => expect(next).toBeDefined(),
               fail
           );
           expect(repositoryMock.find.calls.count()).toBe(1);
           expect(repositoryMock.find).toHaveBeenCalledWith(
               {select: ["id", "username", "firstName", "lastName", "email", "role", "enabled", "memberId"]}
           );
       })
    });
});
