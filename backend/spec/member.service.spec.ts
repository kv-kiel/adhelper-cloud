import * as typeorm from "typeorm";
import * as sinon from "sinon";
import {MemberService} from "../src/services/member.service";
import * as fs from "fs";
import {createReadStream, ReadStream} from "fs";

describe('MemberService', () => {
    let service: MemberService;
    let repositoryMock: {save: jasmine.Spy};


    beforeAll(() => {
        repositoryMock = jasmine.createSpyObj('Repository', ['save']);
        sinon.replace(typeorm, 'getRepository', () => {return repositoryMock as any});
        sinon.replace(fs, 'existsSync', (path) => {return path === 'fileExists'});
        service = new MemberService();
    });

    afterAll(() => {
       sinon.restore();
    });

    describe('parseBaseData', () => {
       it('should emit error if file not exists', () => {
           spyOn(fs, 'createReadStream');
           service.parseBaseData('notExists').subscribe(
               next => fail('Expected error'),
               error => expect(error.message).toBe('File not found!')
           )
           expect(fs.createReadStream).not.toHaveBeenCalled();
       });
    });
});

const mockCsv: string =
    'Mitglieds_Nr;Verknüpfung;Anrede;Vorname;Nachname;Geburtsdatum;Eintritt;Austritt;Beitragsart_1;AD-Frei.Grund;AD-Frei.von;AD-Frei.bis\n' +
    '12345;10101;Frau;Claudia;Hansen;16.07.1990;01.08.2001;31.12.2099;JUGENDL U18, mit Boot;;00.00.0000;00.00.0000\n' +
    '10101;;Herr;Heinrich;Hansen;04.04.1950;01.08.2001;31.12.2099;FAM mit Boot, Kind U16;MANAGEMENT;01.01.2015;31.12.2099\n';
