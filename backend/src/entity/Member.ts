import {Entity, Column, PrimaryColumn} from "typeorm";
import {IMember} from "../models/member";

@Entity()
export class Member implements IMember{
    @PrimaryColumn()
    memberId!: number;

    @Column({nullable: true})
    linkedId?: number;

    @Column()
    title!: string;

    @Column()
    firstName!: string;

    @Column()
    lastName!: string;

    @Column()
    dateOfBirth!: Date;

    @Column()
    dateOfEntry!: Date;

    @Column()
    dateOfResignation!: Date;

    @Column()
    typeOfMembershipFee!: string;

    @Column({nullable: true})
    exemptReason?: string;

    @Column({nullable: true})
    exemptFrom?: Date;

    @Column({nullable: true})
    exemptUntil?: Date;

    fromParsedData(parsedData: {
        memberId: string,
        linkedId: string,
        title: string,
        firstName: string,
        lastName: string,
        dateOfBirth: string,
        dateOfEntry: string,
        dateOfResignation: string,
        typeOfMembershipFee: string,
        exemptReason: string,
        exemptFrom: string | undefined,
        exemptUntil: string | undefined
    }): void {
        const linkedId = Number(parsedData.linkedId);

        this.memberId = Number(parsedData.memberId);
        this.linkedId = linkedId === NaN || linkedId === 0 ? undefined : linkedId;
        this.title = parsedData.title;
        this.firstName = parsedData.firstName;
        this.lastName = parsedData.lastName;
        this.dateOfBirth = this.parseDate(parsedData.dateOfBirth);
        this.dateOfEntry = this.parseDate(parsedData.dateOfEntry);
        this.dateOfResignation = this.parseDate(parsedData.dateOfResignation);
        this.typeOfMembershipFee = parsedData.typeOfMembershipFee;
        this.exemptReason = parsedData.exemptReason.length > 0 ? parsedData.exemptReason : undefined;
        this.exemptFrom = this.exemptReason === undefined || parsedData.exemptFrom === undefined
            ? undefined
            : this.parseDate(parsedData.exemptFrom);
        this.exemptUntil = this.exemptReason === undefined || parsedData.exemptUntil === undefined
            ? undefined
            : this.parseDate(parsedData.exemptUntil);
    }

    private parseDate(dateString: string): Date {
       const date = new Date(0);
       const splitted = dateString.split('.');
       date.setFullYear(Number(splitted[2]));
       date.setMonth(Number(splitted[1]) - 1);
       date.setDate(Number(splitted[0]));
       return date;
    }

}
