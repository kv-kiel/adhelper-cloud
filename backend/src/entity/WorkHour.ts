import {Entity, Column, PrimaryColumn, ManyToOne, JoinColumn} from "typeorm";
import {Member} from "./Member";

@Entity()
export class WorkHour {
    @PrimaryColumn()
    id!: number;

    @Column()
    date!: Date;

    @ManyToOne(type => Member)
    @JoinColumn()
    member!: Member;

    @Column()
    hours!: number;
}
