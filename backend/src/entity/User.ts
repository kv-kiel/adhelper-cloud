import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn, UpdateDateColumn,
} from "typeorm";
import {Length} from "class-validator";
import * as bcrypt from "bcryptjs";
import {IUser, UserRole} from "../models/user";

@Entity()
export class User implements IUser{
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({unique: true})
    @Length(4, 20)
    username!: string;

    @Column({select: false})
    private password!: string;

    @Column()
    firstName!: string;

    @Column()
    lastName!: string;

    @Column()
    email!: string;

    @Column({unique: true, nullable: true})
    memberId?: string;

    @Column({default: false})
    enabled?: boolean;

    @Column({default: 'user'})
    role?: UserRole;

    @Column({default: false})
    memberIdApproved?: boolean

    @Column()
    @CreateDateColumn()
    createdAt!: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt!: Date;

    /**
     * Hashes and updates the password for this entity in the Database
     * @param password: unencrypted password
     */
    public setPassword(password: string): void {
        // Todo: Implement server side validation
        this.password = bcrypt.hashSync(password, 8);
    }

    /**
     * Checks if the given password matches the stored hash
     * @param password: unencrypted password
     */
    public checkPassword(password: string): boolean {
        return bcrypt.compareSync(password, this.password);
    }
}
