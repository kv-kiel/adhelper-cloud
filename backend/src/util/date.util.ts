export class DateUtil {
    public static formatDateString(date: Date): string {
        const year: string = date.getFullYear().toString();
        let month: number | string = date.getMonth() + 1;
        let day: number | string = date.getDate();

        month = month > 9 ? month.toString() : '0' + month;
        day = day > 9 ? day.toString() : '0' + day;

        return year + '-' + month + '-' + day;
    }

    public static addDays(date: Date, days: number): Date {
        return new Date(date.getFullYear(), date.getMonth(),date.getDate() + 1);
    }
}
