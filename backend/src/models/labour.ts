import {Member} from "../entity/Member";

export interface IWorkHour {
   id?: number;
   date: Date;
   member?: Member;
   memberId?: number;
   hours: number;
}

export interface IAttendee {
   id?: number;
   date: Date;
   hours: number;
   member: {
      memberId: number;
      firstName: string;
      lastName: string;
   };
}
