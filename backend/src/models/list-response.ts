
export interface IListResponse<T> {
    from: number;
    take: number;
    total: number;
    data: Array<T>;
}
