export interface IMember {
    memberId?: number;
    linkedId?: number;
    title?: string;
    firstName?: string;
    lastName?: string;
    dateOfBirth?: Date;
    dateOfEntry?: Date;
    dateOfResignation?: Date;
    typeOfMembershipFee?: string;
    exemptReason?: string;
    exemptFrom?: Date;
    exemptUntil?: Date;
}


