export interface IUser {
    id?: number;
    username: string;
    newPassword?: string;
    firstName: string;
    lastName: string;
    email: string;
    memberId?: string;
    enabled?: boolean;
    role?: UserRole;
    memberIdApproved?: boolean;
}

export enum UserRole {
    Admin = 'admin',
    User = 'user',
    Assessor = 'assessor',
    Typist = 'typist',
}
