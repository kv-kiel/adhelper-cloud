import 'reflect-metadata';
import 'dotenv/config';
import * as express from 'express';
import {createConnection} from 'typeorm';
import LoggerService from "./services/logger.service";
import {Inject} from "typescript-ioc";
import {Express} from "express";
import {Route} from "./route";
import {ConfigurationService} from "./services/configuration.service";

/**
 * The main class is used for basic startup of express server and database connection.
 * We need a class here in order to use dependency injection.
 */
class Main {
    @Inject private logger!: LoggerService;
    @Inject private route!: Route;
    @Inject private config!: ConfigurationService;

    private app: Express = express();

    public connectDatabase() {
            createConnection()
                .then( () => this.logger.log.info('Database connected'))
                .catch(error => this.logger.log.error(error));
    }

    public startExpressServer() {
        this.app.use(express.json());
        this.app.use('/', this.route.root);
        this.app.listen(this.config.apiPort, () => {
            this.logger.log.info('Server running and listening on port: ' + this.config.apiPort);
        });

    }
}

const main: Main = new Main();

main.connectDatabase();
main.startExpressServer();
