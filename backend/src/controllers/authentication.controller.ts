import {Request, Response, Router} from "express";
import {Inject} from "typescript-ioc";
import {AuthenticationService} from "../services/authentication.service";

export class AuthenticationController {

    public readonly route: Router = Router();
    @Inject private service!: AuthenticationService;

    constructor() {
        this.route.post('/login', this.login);
        this.route.use('/token', [this.service.checkToken]);
        this.route.get('/token', this.refreshToken);
    }

    private login = (req: Request, res: Response) => {
        let {username, password} = req.body;
        if (!username || !password) {
            res.status(400).send();
        }

        this.service.login(username, password).subscribe(
            (token: string) => {
                    res.send({token: token});
                },
            error => res.status(401).send()
        );
    }

    private refreshToken = (req: Request, res: Response) => {
        this.service.refreshToken(res.locals.username).subscribe(
            (token: string) => {
                res.send({token: token});
            },
            error => res.status(401).send()
        );
    }
}
