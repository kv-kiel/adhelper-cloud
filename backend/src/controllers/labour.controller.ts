import {Request, Response, Router} from "express";
import {Inject} from "typescript-ioc";
import LoggerService from "../services/logger.service";
import {RestStatus} from "../models/rest-status";
import {LabourService} from "../services/labour.service";
import {IWorkHour} from "../models/labour";
import {AuthenticationService} from "../services/authentication.service";
import {UserRole} from "../models/user";

export class LabourController {

    public readonly route: Router = Router();
    @Inject private service!: LabourService;
    @Inject private loggerService!: LoggerService;
    @Inject auth! : AuthenticationService;

    constructor() {
        this.route.get('/', this.getAll);
        this.route.post('/', [this.auth.checkRole([UserRole.Admin, UserRole.Typist])], this.create);
        // this.route.patch('/:id', this.patch);
        this.route.delete('/:id', this.delete);
        this.route.get('/attendees', this.getAttendees)
        this.route.get('/member/:id', this.getMemberHours)
    }

    private getAll = (req: Request, res: Response) => {
        this.service.getAll().subscribe(
            next => res.send(next),
            err => res.status(500).send(err)
        )
    }

    private patch = (req: Request, res: Response) => {
        const id: number = Number.parseInt(req.params.id);

        if (isNaN(id)) {
            res.status(RestStatus.NOT_FOUND).send();
            return;
        }

        this.service.patch(id, req.body).subscribe(
            () => res.status(RestStatus.NO_CONTENT).send(),
            err => {
                let status : number = RestStatus.INTERNAL_SERVER_ERROR;
                this.loggerService.log.error(err.name);
                if (err.name = 'EntityNotFound') {
                    status = RestStatus.NOT_FOUND;
                }
                res.status(status).send();
                return;
            }
        );
    }

    private getAttendees = (req: Request, res: Response) => {
        const beginString: string = req.query.fromDate as string ?? undefined;
        const begin: Date | undefined = beginString ? new Date(beginString) : undefined;
        const endString: string = req.query.toDate as string ?? undefined;
        const end: Date | undefined = endString ? new Date(endString) : undefined;

        this.service.getAttendees(begin, end).subscribe(
            next => res.send(next),
            err => res.status(500).send(err)
        )
    }

    private getMemberHours = (req: Request, res: Response) => {
        const id: number = Number.parseInt(req.params.id);

        if (isNaN(id)) {
            res.status(RestStatus.NOT_FOUND).send();
            return;
        }

        const beginString: string = req.query.fromDate as string ?? undefined;
        const begin: Date | undefined = beginString ? new Date(beginString) : undefined;
        const endString: string = req.query.toDate as string ?? undefined;
        const end: Date | undefined = endString ? new Date(endString) : undefined;

        this.service.getHoursForAttendee(id, begin, end).subscribe(
            next => res.send(next),
            err => res.status(500).send(err)
        )
    }

    private create = (req: Request, res: Response) => {
        const workHour: IWorkHour = req.body;

        if(
            workHour === undefined ||
            workHour.date === undefined ||
            workHour.memberId === undefined ||
            workHour.hours === undefined
        ) {
            res.status(RestStatus.BAD_REQUEST).send();
            return;
        }

        this.service.create(workHour).subscribe(
            next => res.location(req.originalUrl + '/' + next.id).status(RestStatus.CREATED).send(),
            error => {
                this.loggerService.log.error(error);
                let status: RestStatus = RestStatus.INTERNAL_SERVER_ERROR;
                if (error.message === 'Doppelter Eintrag') {
                    status = RestStatus.CONFLICT;
                }
                res.status(status).send(error.message);    // Internal Server Error
                return;
            }
        )
    }

    private delete = (req: Request, res: Response) => {
        const id: number = Number.parseInt(req.params.id);

        if (isNaN(id)) {
            res.status(RestStatus.NOT_FOUND).send();
            return;
        }

        this.service.dropWorkHour(id).subscribe(
            result => {
                res.status(RestStatus.NO_CONTENT).send();
                return;
            },
            err => {
                let status : number = RestStatus.INTERNAL_SERVER_ERROR;
                this.loggerService.log.error(err.name);
                if (err.name = 'EntityNotFound') {
                    status = RestStatus.NOT_FOUND;
                }
                res.status(status).send();
                return;
            });
    }
}
