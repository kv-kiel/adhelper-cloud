import {Request, Response, Router} from "express";
import {Inject} from "typescript-ioc";
import {UserService} from "../services/user.service";
import LoggerService from "../services/logger.service";
import {IUser} from "../models/user";
import {RestStatus} from "../models/rest-status";

export class UserController {

    public readonly route: Router = Router();
    @Inject private service!: UserService;
    @Inject private loggerService!: LoggerService;

    constructor() {
        this.route.get('/', this.listUser);
        this.route.post('/', this.createUser);
        this.route.get('/:id', this.getUser);
        this.route.delete('/:id', this.deleteUser);
        this.route.patch('/:id', this.patchUser);
    }

    private listUser = (req: Request, res: Response) => {
        this.service.getAll().subscribe(
            users => res.send(users),
            err => res.status(500).send()
        )
    }

    private patchUser = (req: Request, res: Response) => {
        const id: number = Number.parseInt(req.params.id);

        this.service.patch(id, req.body, res.locals.userRole).subscribe(
            () => res.status(RestStatus.NO_CONTENT).send(),
            err => {
                let status : number = RestStatus.INTERNAL_SERVER_ERROR;
                this.loggerService.log.error(err.name);
                if (err.name = 'EntityNotFound') {
                    status = RestStatus.NOT_FOUND;
                }
                res.status(status).send();
                return;
            }
        );
    }

    // Todo: Sollte dies eine zusätzliche Authentifizierung erfordern?
    private createUser = (req: Request, res: Response) => {
        const user: IUser = req.body;

        if(
            user.username === undefined ||
            user.firstName === undefined ||
            user.lastName === undefined ||
            user.email === undefined ||
            user.newPassword === undefined
        ) {
            res.status(RestStatus.BAD_REQUEST).send();
            return;
        }

        this.service.insertUser(user).subscribe(
            next =>
                res.location(req.originalUrl + '/' + next.id).status(RestStatus.CREATED).send(),
            error => {
                this.loggerService.log.error(error);
                if(error.message.includes('ER_DUP_ENTRY') || error.message.includes('SQLITE_CONSTRAINT: UNIQUE')) {
                    let message = error.message.includes(user.username) || error.message.includes('username')
                        ? 'Benutzername ' + user.username
                        : 'Mitgliedsnummer ' + user.memberId;
                    message += ' ist bereits vergeben.';
                    res.status(RestStatus.CONFLICT).send(message);      // Conflict
                    return;
                }
                res.status(RestStatus.INTERNAL_SERVER_ERROR).send(error.message);    // Internal Server Error
                return;
            }
        )
    }

    // Todo: Sollte dies eine zusätzliche Authentifizierung erfordern?
    // Todo: Sollte es ausschlüsse geben (Eigene ID, ID 1, etc.)?
    private deleteUser = (req: Request, res: Response) => {
        const id: number = Number.parseInt(req.params.id);

        if (isNaN(id)) {
            res.status(RestStatus.NOT_FOUND).send();
            return;
        }

        this.service.dropUser(id).subscribe(
            result => {
               res.status(RestStatus.NO_CONTENT).send();
               return;
            },
            err => {
                let status : number = RestStatus.INTERNAL_SERVER_ERROR;
                this.loggerService.log.error(err.name);
                if (err.name = 'EntityNotFound') {
                    status = RestStatus.NOT_FOUND;
                }
                res.status(status).send();
                return;
            });
    }

    private getUser = (req: Request, res: Response) => {
        const id: number = Number.parseInt(req.params.id);

        if (isNaN(id)) {
            res.status(RestStatus.NOT_FOUND).send();
            return;
        }

        this.service.getById(id).subscribe(
            user => {
                res.status(RestStatus.OK).send(user);
                return;
            },
            err => {
                let status : number = RestStatus.INTERNAL_SERVER_ERROR;
                this.loggerService.log.error(err.name);
                if (err.name = 'EntityNotFound') {
                    status = RestStatus.NOT_FOUND;
                }
                res.status(status).send();
                return;
            });
    }
}
