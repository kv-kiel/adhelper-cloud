import {NextFunction, Request, Response, Router} from "express";
import {Inject} from "typescript-ioc";
import LoggerService from "../services/logger.service";
import {File, IncomingForm} from "formidable";
import {RestStatus} from "../models/rest-status";
import {MemberService} from "../services/member.service";
import {IListResponse} from "../models/list-response";
import {IMember} from "../models/member";
import {UserRole} from "../models/user";
import {AuthenticationService} from "../services/authentication.service";

export class MemberController {

    public readonly route: Router = Router();
    @Inject private service!: MemberService;
    @Inject private logger!: LoggerService;
    @Inject private auth!: AuthenticationService;

    constructor() {
        this.route.use('/upload', [this.parseMultipartForm] );
        this.route.post('/upload',
            [this.auth.checkRole([UserRole.Admin, UserRole.Assessor])],
            this.upload);
        this.route.get('/',
            [this.auth.checkRole([UserRole.Admin, UserRole.Assessor, UserRole.Typist])],
            this.listMember);
        this.route.get('/find', this.findMember);
    }

    private listMember = (req: Request, res: Response) => {
        let from: number = Number(req.query.from ?? 0);
        let take: number | undefined = Number(req.query.take);


        this.service.getAll().subscribe(
            next => {
                const total = next.length;
                const data = next.slice(from, take ? from + take : undefined);
                const response: IListResponse<IMember> = { data, total, from, take: data.length};
                res.send(response);
            },
            err => res.status(500).send()
        )
    }

    private findMember = (req: Request, res: Response) => {
        let name: string = req.query.name as string ?? '';

        this.service.getLike(name).subscribe(
            next => {
                const response: IListResponse<IMember> = { data: next, total: next.length, from: 0, take: next.length};
                res.send(response);
            },
            err => res.status(500).send()
        );
    }

    private upload = (req: Request, res: Response) => {
        if(res.locals.file === undefined) {
            this.logger.log.error('No file found');
            res.status(RestStatus.BAD_REQUEST);
            res.send();
            return;
        }

        this.service.parseBaseData(res.locals.file.path).subscribe(
            next => res.status(RestStatus.OK).send(next),
            error => res.status(RestStatus.INTERNAL_SERVER_ERROR).send(error.message)
        );

    }

    /**
     * Parse incoming multipart forms ans set information and location of uploaded file
     * in res.locals.file
     * To be used as express middleware function
     */
    private parseMultipartForm = (req: Request, res: Response, next: NextFunction) => {

        // Early exit in case of repeated calls
        if (res.locals.file !== undefined) {
            next();
        }

        this.logger.log.debug('Processing incoming Form ...');

        // @ts-ignore
        let form = new IncomingForm();
        form.parse(req);

        form.once('error', (err) => {
            this.logger.log.error(err);
            next();
        });

        form.on('file', (name: string, file: File) => {
            res.locals.file = file;
        });

        form.once('end', () => {
            this.logger.log.debug('... done!');
            next();
        });
    }
}

