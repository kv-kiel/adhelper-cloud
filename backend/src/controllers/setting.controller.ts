import {Request, Response, Router} from "express";
import {Inject} from "typescript-ioc";
import {UserService} from "../services/user.service";
import LoggerService from "../services/logger.service";

export class SettingController {

    public readonly route: Router = Router();
    @Inject private service!: UserService;
    @Inject private loggerService!: LoggerService;

    constructor() {
        this.route.post('/password', this.changePassword);
    }

    private changePassword = (req: Request, res: Response) => {
        let {oldPassword, newPassword} = req.body;

        // Request misses needed data => send BAD REQUEST
        if (!oldPassword || !newPassword) {
            res.status(400).send();
        }

        let username = res.locals.username;

        this.service.changePassword(username, oldPassword, newPassword).subscribe(
                // Observable fires = all went well => send NO CONTENT
            () => res.status(204).send(),
            err => {
                // Error 'Forbidden' was thrown = oldPassword invalid => send FORBIDDEN
                if (err.message === 'Forbidden') {
                    res.status(403).send()
                }
                // Else error thrown => send INTERNAL SERVER ERROR
                res.status(500).send()
            }
        );
    }
}
