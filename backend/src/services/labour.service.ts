import LoggerService from "./logger.service";
import {from, Observable} from "rxjs";
import {Inject} from "typescript-ioc";
import {getRepository, Repository} from "typeorm";
import {ConfigurationService} from "./configuration.service";
import {WorkHour} from "../entity/WorkHour";
import {map, switchMap, take, tap} from "rxjs/operators";
import {MemberService} from "./member.service";
import {IAttendee, IWorkHour} from "../models/labour";
import {Member} from "../entity/Member";
import {DateUtil} from "../util/date.util";

export class LabourService {
    private repository: Repository<WorkHour> = getRepository(WorkHour);
    @Inject private logger!: LoggerService;
    @Inject private config!: ConfigurationService;
    @Inject private memberService!: MemberService;

    public getAll(): Observable<WorkHour[]> {
        return from(this.repository.find({relations: ['member']}));
    }

    public getAttendees(dateFrom: Date | undefined, dateTo: Date | undefined): Observable<IAttendee[]> {
        const start: string = dateFrom ? DateUtil.formatDateString(new Date(dateFrom)) : DateUtil.formatDateString(new Date(0));
        const end: string = (dateTo ? DateUtil.formatDateString(new Date(dateTo)) : DateUtil.formatDateString(new Date()) ) + ' 23:59:59.999';
        return from(
            this.repository.createQueryBuilder("w")
                .leftJoinAndSelect("w.member", "m")
                .select(["w.id", "w.date", "m.memberId", "m.firstName", "m.lastName", "w.hours"])
                .where("w.date between :start and :end", {start, end})
                .getMany()
        );
    }

    public getHoursForAttendee(memberId: number, dateFrom: Date | undefined, dateTo: Date | undefined): Observable<WorkHour[]> {
        const start: string = dateFrom ? DateUtil.formatDateString(new Date(dateFrom)) : DateUtil.formatDateString(new Date(0));
        const end: string = (dateTo ? DateUtil.formatDateString(new Date(dateTo)) : DateUtil.formatDateString(new Date()) ) + ' 23:59:59.999';
        return  from(
            this.repository.createQueryBuilder("w")
                .where("w.member = :member", {member: memberId})
                .andWhere("w.date between :start and :end", {start, end})
                .getMany()
        );
    }

    public create(workHour: IWorkHour): Observable<WorkHour> {
        let entity: WorkHour = new WorkHour();
        entity.date = workHour.date;
        entity.hours = workHour.hours;

        return this.getHoursForAttendee(workHour.memberId ?? 0, workHour.date, workHour.date).pipe(
            tap(next => {
                if (next.length > 0) {
                   throw new Error('Doppelter Eintrag');
                }
            }),
            switchMap(() => from(this.memberService.getById(workHour.memberId ?? 0))),
            take(1),
            map((next: Member) => {
                entity.member = next;
                return entity;
            }),
            switchMap((next: WorkHour) => from(this.repository.save(next)))
        );
    }

    public patch(id: number, workHour: IWorkHour): Observable<void> {
        return from(this.repository.findOneOrFail({id})).pipe(
            map((entity: WorkHour) => {
               entity.hours = workHour.hours;
               return entity;
            }),
            switchMap((entity) => from(this.repository.save(entity))),
            map(() => {})
        );
    }

    public dropWorkHour(id: number): Observable<void> {
        return from(this.repository.findOneOrFail({id: id})).pipe(
            switchMap(() => from(this.repository.delete({id: id}))),
            map(() => {})
        )
    }
}

