import {Logger} from 'tslog';
import {Singleton} from "typescript-ioc";
import {NextFunction, Request, Response} from "express";

/**
 * The LoggerService wraps the tslog Logger in order to get less dependant on
 * the specific calls of that library. Also it uses a single instance of the logger
 * and can be injected.
 */
@Singleton
export default class LoggerService {
    private readonly _logger: Logger;

    constructor() {
        this._logger = new Logger();
    }

    public get log(): Logger {
        return this._logger;
    }

    /**
     * Middleware function for express which logs requests
     * @param req
     * @param res
     * @param next
     */
    public logRequest = (req: Request, res: Response, next: NextFunction) => {
        this._logger.info(req.method + ':' + req.originalUrl + ' by ' + req.headers.host);
        next();
    }
}
