import {Singleton} from "typescript-ioc";


/**
 * This services loads config parameters from .env and stores them
 * to config variables.
 * By injecting this services, the configurations can easily be accessed
 * from any file.
 */
@Singleton
export class ConfigurationService {

    /* Todo: Der Fall, das configurationen  nicht geladen werden können,
     * im speziellen auch das Secret, muss abgefangen werden.
     */

    public readonly jwt!: JwtConfig;
    public readonly apiPort!: string;
    public readonly sqlChunkSize!: number;

    constructor() {
        this.jwt = {
            expiry: process.env.JWT_EXPIRY ?? '',
            secret: process.env.JWT_SECRET ?? ''
        };

        this.apiPort = process.env.API_PORT ?? '';

        this.sqlChunkSize = Number(process.env.SQL_CHUNK_SIZE ?? 50);
    }
}

export interface JwtConfig {
    secret: string;
    expiry: string;
}
