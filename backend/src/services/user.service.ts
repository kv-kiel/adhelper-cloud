import {getRepository, Repository} from "typeorm";
import {User} from "../entity/User";
import {from, Observable} from "rxjs";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {Inject} from "typescript-ioc";
import LoggerService from "./logger.service";
import {IUser, UserRole} from "../models/user";

export class UserService {
    private repository: Repository<User> = getRepository(User);
    @Inject private loggerService!: LoggerService;

    public getByNameWithPassword(name: string): Observable<User> {
        return from(this.repository.createQueryBuilder()
            .where({username: name})
            .addSelect('User.password')
            .getOne()).pipe(
                map(user => {
                    if (user) {
                        return user;
                    }
                    throw new Error();
                })
        );
        // return  from(this.repository.findOneOrFail({username: name}));
    }

    public getAll(): Observable<IUser[]> {
        return from(
            this.repository.find({
                select: ["id", "username", "firstName", "lastName", "email", "role", "enabled", "memberId"]
            })
        );
    }

    public patch(id: number, user: IUser, requesterRole?: UserRole): Observable<void> {
        return  from(this.repository.findOneOrFail({id: id})).pipe(
            map((entity: User) => {
                if (user.username !== undefined) {
                    entity.username = user.username;
                }
                if (user.firstName !== undefined) {
                    entity.firstName = user.firstName;
                }
                if (user.lastName !== undefined) {
                    entity.lastName = user.lastName;
                }
                if (user.email !== undefined) {
                    entity.email = user.email;
                }
                if (user.memberId !== undefined && entity.memberIdApproved === false) {
                    entity.memberId = user.memberId;
                }
                if (requesterRole && requesterRole === UserRole.Admin) {
                    if (user.memberIdApproved !== undefined) {
                        entity.memberIdApproved = user.memberIdApproved;
                    }
                    if (user.enabled !== undefined) {
                        entity.enabled = user.enabled;
                    }
                    if (user.role !== undefined) {
                        entity.role = user.role;
                    }
                }
                return entity
            }),
            switchMap((entity: User) => from(this.repository.save(entity))),
            tap(next => this.loggerService.log.debug(next)),
            map(() => {})
        );
    }

    public dropUser(id: number): Observable<void> {
        return from(this.repository.findOneOrFail({id: id})).pipe(
            switchMap(() => from(this.repository.delete({id: id}))),
            map(() => {})
        )
    }

    public getById(id: number): Observable<IUser> {
        return  from(this.repository.findOneOrFail({id: id}));
    }

    public insertUser(user: IUser): Observable<IUser> {
        let entity: User = new User();
        entity.username = user.username;
        entity.firstName = user.firstName;
        entity.lastName = user.lastName;
        entity.email = user.email;
        entity.enabled = user.enabled;
        entity.memberIdApproved = user.memberIdApproved;
        entity.memberId = user.memberId;
        entity.role = user.role;
        if (user.newPassword) {
            entity.setPassword(user.newPassword);
        }
        return from(this.repository.save(entity)).pipe(map(saved => saved as IUser));
    }

    public changePassword(username: string, oldPassword: string, newPassword: string): Observable<boolean> {
        return this.getByNameWithPassword(username).pipe(
            // Validate oldPassword and intercept further processing by throwing error when invalid
            tap((user: User) => {
                if (!user.checkPassword(oldPassword)) {
                    throw new Error('Forbidden');
                }
            }),
            // Set and hash the new password to the entity in memory
            map((user: User) => {
                user.setPassword(newPassword);
                return user;
            }),
            // Save the entity to the database
            switchMap((user: User) => from(this.repository.save(user))),
            // If we reach here, everything was successful => return true
            map(() => true),
            // Log and rethrow errors
            catchError(err => {
                this.loggerService.log.error(err);
                throw err;
            })
        );
    }
}
