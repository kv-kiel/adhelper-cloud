import LoggerService from "./logger.service";
import {EMPTY, from, Observable, of, Subject, throwError} from "rxjs";
import * as fs from "fs";
import {Inject} from "typescript-ioc";
import * as csv from 'fast-csv';
import {RowMap} from "@fast-csv/parse";
import {Member} from "../entity/Member";
import {getRepository, Repository} from "typeorm";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {ConfigurationService} from "./configuration.service";
import {IParseResult} from "../models/parse-result";


export class MemberService {
    private repository: Repository<Member> = getRepository(Member);
    @Inject private logger!: LoggerService;
    @Inject private config!: ConfigurationService;

    regexNumber: RegExp = new RegExp('^[+ 0-9]{5}$');
    regexDate: RegExp = new RegExp('^\\s*(3[01]|[12][0-9]|0[1-9])\\.(1[012]|0[1-9])\\.((?:19|20)\\d{2})\\s*$');
    regexName: RegExp = new RegExp('^\\D*$');

    public getAll(): Observable<Member[]> {
        return from(this.repository.find());
    }

    public getById(id: number): Observable<Member> {
        return  from(this.repository.findOneOrFail({memberId: id}));
    }

    public getLike(name: string): Observable<Member[]> {
        name = '%' + name + '%';
        return from(
            this.repository.createQueryBuilder()
                .where("member.firstName like :name", {name})
                .orWhere("member.lastName like :name", {name})
                .getMany()
        );
    }

    /**
     * Parses baseData csv with base validation and creates Member for each row.
     * The IParseResult is emitted after the parsing is done, the database routines are
     * called afterwards!
     * @param pathToFile
     * @return Observable of IParseResult, number of parsed and invalid rows and the indices of invalid rows
     */
    public parseBaseData(pathToFile: string): Observable<IParseResult> {
        const result: Subject<IParseResult> = new Subject<IParseResult>();
        let validCount: number = 0;
        let invalidCount: number = 0;
        let invalidRows: number[] = new Array();
        let members: Member[] = new Array();

        // Early exit
        if (!fs.existsSync(pathToFile)) {
            this.logger.log.error(`File ${pathToFile} not found!`);
            return throwError(new Error(`File not found!`));
        }

        fs.createReadStream(pathToFile, {encoding: 'latin1'})
            .pipe(csv.parse({ headers, renameHeaders: true, delimiter: ';', strictColumnHandling: true})
            )
            .validate((row: RowMap) => {
                return this.regexNumber.test(row.memberId)
                    && this.regexName.test(row.firstName)
                    && this.regexName.test(row.lastName)
                    && this.regexDate.test(row.dateOfBirth)
                    && this.regexDate.test(row.dateOfEntry)
                    && this.regexDate.test(row.dateOfResignation)
                    ;
            })
            .on('data-invalid', row => {
                invalidCount++;
                const index = validCount + invalidCount;
                invalidRows.push(index);
                this.logger.log.debug(`Found invalid row: ${index}`);
            })
            .on('error', error => {
                this.logger.log.error(error);
                result.error(error);
                result.complete();
                return;
            })
            .on('data', row => {
                // this.logger.log.debug(row);
                validCount++;
                const member: Member = new Member();
                if (!this.regexDate.test(row.exemptFrom)) {
                    row.exemptFrom = undefined;
                }
                if (!this.regexDate.test(row.exemptUntil)) {
                    row.exemptUntil = undefined;
                }
                member.fromParsedData(row);
                members.push(member);
            })
            .on('end', (rowCount: number) => {
                this.logger.log.info(`Parsed ${validCount} datasets out of ${rowCount} rows`);
                result.next({rowCount, invalidRows, invalidCount});
                result.complete();
                // The database handling is called here, after the result having been emitted!!!
                this.updateRecords(members).subscribe(
                    () => this.logger.log.info('Done!'),
                    err => this.logger.log.error(err));
            });

        return result.asObservable();
    }

    /**
     * Takes an array of Member entities to be updated or inserted in to the database.
     * The array is passed in chunks to the database repository to avoid errors
     * @param members
     */
    private updateRecords(members: Member[]): Observable<number> {
        if (members.length < 1) {
            return of(0);
        }
        const chunk: Member[] = members.slice(0, this.config.sqlChunkSize);

        return from(this.repository.save(chunk)).pipe(
            map(next => next.length),
            tap(next => this.logger.log.debug(next + ' records saved')),
            switchMap(next => this.updateRecords(members.slice(next))),
        );
    }
}

/*
 * Attribute names for the objects parsed from the csv file
 */
const headers = [
    'memberId',
    'linkedId',
    'title',
    'firstName',
    'lastName',
    'dateOfBirth',
    'dateOfEntry',
    'dateOfResignation',
    'typeOfMembershipFee',
    'exemptReason',
    'exemptFrom',
    'exemptUntil'
]
