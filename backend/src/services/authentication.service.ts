import {Observable} from "rxjs";
import * as jwt from "jsonwebtoken";
import {Inject} from "typescript-ioc";
import {ConfigurationService} from "./configuration.service";
import {User} from "../entity/User";
import {UserService} from "./user.service";
import {catchError, map, tap} from "rxjs/operators";
import LoggerService from "./logger.service";
import {NextFunction, Request, Response} from "express";

export class AuthenticationService {

    @Inject private config!: ConfigurationService;
    @Inject private userService!: UserService;
    @Inject private logger!: LoggerService;

    public login(username: string, password: string): Observable<string> {
        return this.userService.getByNameWithPassword(username).pipe(
            map((user: User) => {
                if (!user.enabled) {
                    throw new Error('User disabled');
                }
                if (user.checkPassword(password)) {
                    return this.computeToken(user);
                }
                throw new Error('wrong password');
            }),
            catchError(err => {
                this.logger.log.warn('Rejected login attempt by ' + username + ' : ' + err.message);
                throw err;
            })
        );
    }

    public refreshToken(username: string): Observable<string> {
        return this.userService.getByNameWithPassword(username).pipe(
            tap(user => {
                if(!user.enabled) {
                    this.logger.log.warn('Rejected token refresh by ' + username + ' : disabled');
                    throw new Error('User disabled');
                }
            }),
            map( user => this.computeToken(user))
       );
    }

    public checkToken = (req: Request, res: Response, next: NextFunction) => {
        let token: string = req.headers["authorization"] ?? '';
        token = token.replace('Bearer ', '');

        try {
            let decoded: any = jwt.verify(token, this.config.jwt.secret);
            res.locals.username = decoded.username;
            res.locals.userRole = decoded.userRole;
        }
        catch (err) {
            //If token is not valid, respond with 401 (unauthorized)
            this.logger.log.warn('Unauthorized request: ' + err);
            res.status(401).send(err);
            return;
        }
        next();
    }

    public checkRole = (roles: Array<string>) => {
        return async (req: Request, res: Response, next: NextFunction) => {
            //Get the user ID from previous midleware
            const role = res.locals.userRole;

            if (roles.includes(role)) {
                next();
            } else {
                this.logger.log.warn('Unerlaubter Zugriffsversuch durch ' + res.locals.username);
                res.status(403).send();
            }
        }
    }

    private computeToken(user: User): string {
        return jwt.sign({username: user.username, userRole: user.role}, this.config.jwt.secret, {expiresIn: this.config.jwt.expiry});
    }
}
