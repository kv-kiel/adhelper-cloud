import {MigrationInterface, QueryRunner} from "typeorm";

export class Member1603899445105 implements MigrationInterface {
    name = 'Member1603899445105'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "member" ("memberId" integer PRIMARY KEY NOT NULL, "linkedId" integer, "title" varchar NOT NULL, "firstName" varchar NOT NULL, "lastName" varchar NOT NULL, "dateOfBirth" datetime NOT NULL, "dateOfEntry" datetime NOT NULL, "dateOfResignation" datetime NOT NULL, "typeOfMembershipFee" varchar NOT NULL, "exemptReason" varchar, "exemptFrom" datetime, "exemptUntil" datetime)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "member"`);
    }

}
