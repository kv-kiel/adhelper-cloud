import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import {User} from "../../entity/User";
import {UserRole} from "../../models/user";

export class InsertAdmin1602002564559 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let user = new User();
        user.username = 'admin';
        user.firstName = 'none';
        user.lastName = 'none';
        user.email = 'none';
        user.enabled = true;
        user.role = UserRole.Admin;

        user.setPassword('admin');

        const repository = getRepository(User);
        await repository.save(user);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
