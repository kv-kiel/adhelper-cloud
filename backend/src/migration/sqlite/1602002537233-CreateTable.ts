import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateTable1602002537233 implements MigrationInterface {
    name = 'CreateTable1602002537233'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "username" varchar NOT NULL, "password" varchar NOT NULL, "firstName" varchar NOT NULL, "lastName" varchar NOT NULL, "email" varchar NOT NULL, "memberId" varchar, "enabled" boolean NOT NULL DEFAULT (0), "role" varchar NOT NULL DEFAULT ('user'), "memberIdApproved" boolean NOT NULL DEFAULT (0), "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "UQ_a82e919c7608211cdfa42c57ff6" UNIQUE ("memberId"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
