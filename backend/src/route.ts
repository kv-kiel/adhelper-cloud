import {Router} from "express";
import LoggerService from "./services/logger.service";
import {Inject} from "typescript-ioc";
import {AuthenticationController} from "./controllers/authentication.controller";
import {AuthenticationService} from "./services/authentication.service";
import {UserController} from "./controllers/user.controller";
import {SettingController} from "./controllers/setting.controller";
import {MemberController} from "./controllers/member.controller";
import {UserRole} from "./models/user";
import {LabourController} from "./controllers/labour.controller";

/**
 * Basic routing to map requests to the API endpoint controllers
 */
export class Route {
    @Inject logger!: LoggerService;
    @Inject auth!: AuthenticationService;
    @Inject authenticationController!: AuthenticationController;
    @Inject userController!: UserController;
    @Inject settingController!: SettingController;
    @Inject memberController!: MemberController;
    @Inject labourController!: LabourController;

    public root: Router;

    constructor() {
        this.root = Router();

        this.root.use('/api/authentication', [this.logger.logRequest], this.authenticationController.route);
        this.root.use('/api/setting', [this.logger.logRequest, this.auth.checkToken], this.settingController.route);
        this.root.use(
            '/api/user',
            [this.logger.logRequest, this.auth.checkToken, this.auth.checkRole([UserRole.Admin])],
            this.userController.route
        );
        this.root.use(
            '/api/member',
            [this.logger.logRequest, this.auth.checkToken, this.auth.checkRole([UserRole.Admin, UserRole.Assessor, UserRole.Typist])],
            this.memberController.route
        );
        this.root.use(
            '/api/labour',
            [this.logger.logRequest, this.auth.checkToken, this.auth.checkRole([UserRole.Admin, UserRole.Assessor, UserRole.Typist])],
            this.labourController.route
        );
    }
}
