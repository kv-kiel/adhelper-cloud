# AD Helper Cloud Backend
## Migrations
```
// Generiert migrationen anhand von Schema Änderungen
$ npm run typeorm migration:generate -- -n migrationNameHere

// Erstellt migrationen ohne auto genrierte Statements
$ npm run typeorm migration:creat -- -n migrationNameHere

// Führt migrationen aus
$ npm run typeorm migration:run
```
