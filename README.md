# ADHelper-Cloud

Siehe https://kv-kiel.gitlab.io/kvk-doku-center/ 

## Dev setup
### Node installieren
Wir benötigen node Version 12.18.2. Das geht am besten mit dem nvm (Node Version Manager)
#### nvm installieren
``` 
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
$ source ~/.bashrc	
```
Der zweite Befehl ist eventuell nicht notwendig bzw. lässt sich auch durch einen Neustart des 
Terminals ersetzen.

#### node mit nvm installieren
```
$ nvm ls-remote
$ nvm install 12.18.2
$ node -v
```
Node -v sollte jetzt die korrekte Version ausgeben (v12.18.2)

### Repo auschecken
```
$ git clone https://gitlab.com/kv-kiel/adhelper-cloud.git
```

### Installieren der Abhängigkeiten
Dies muss separat für das Backend und das Frontend geschehen:
```
$ cd backend    
$ npm install   
$ cd ../frontend
$ npm install          
```

### Backend .env Konfiguration
Das Backend verwendet für manche Parameter, wie die Zugangsdaten
zur Datenbank, eine dotenv Konfiguration. Die entsprechende Datei
.env wird nicht commited und gepushed. Sie muss daher lokal
angelegt werden. Dazu lässt sich die template Datei verwenden.
```
$ cp .env.template .env
```
Anschließdend müssen die entsprechenden Werte in der Datei ergänzt werden.

### Backend ormconfig Konfiguration
Auch die Verbindungskonfiguration der Datenbank wird zur Sicherheit nur als
template mit commited und gepushed. Die Template Datei enthält allerdings eine
funktionierende SQLite Konfiguration. Um diese zu nutzen muss auch hier einfach
eine Kopie angelegt werden:
```
$ cp ormconfig.template.json ormconfig.json 
```
Nur zum Verwenden einer MySQL Verbindung müssen hier Werte vorgenommen werden.


### Datenbank vorbereiten
Um das Datenbank Schema zu erzeugen und den Adminuser anzulegen, müssen 
Migrationen ausgeführt werden.
Dies erfolgt im Ordner backend/ mit:
```
$ npm run typeorm migration:run
```

## Ausführen
Im jeweiligen Ordner (backend oder frontend):
```
$ npm start
```
Dies startet das Backend bzw. im Falle des Frontends einen dev Server der das Frontend ausliefert.

